package YComponentsLegacy;

import YComponents.YPanel;
import YComponents.YTheme;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.LinkedList;

public class YChooser extends YPanel
{
	
	LinkedList<YToggleButton> _options;
	//boolean _multiple;
	
	public YChooser(LinkedList<String> Options)
	{
		this(Options, FlowLayout.CENTER);
	}
	
	public YChooser(LinkedList<String> Options, int FlowLayoutOrientation)
	//, boolean MultipleAnswersPossible
	{
		setVisible(false);
		setLayout(new FlowLayout(FlowLayoutOrientation, 5, 5));
		_options = new LinkedList<YToggleButton>();
		
		for(String S : Options)
		{
			YToggleButton current = new YToggleButton(S);
			current.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_LIGHT_3));
			current.setPreferredSize(new Dimension(30 + (S.length() * 8), 30));
			_options.add(current);
			add(current);
			
		}
	}
	
	
	public LinkedList<String> getChosen ()
	{
		
		LinkedList<String> chosen = new LinkedList<String>();
		
		
		for(YToggleButton t : _options)
		{
			if(t.isSelected())
			{
				chosen.add(t.getText());
			}
		}
		
		return chosen;
	}
	
}
