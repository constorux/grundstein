package YComponentsLegacy;

import YComponents.YTheme;

import java.awt.*;
import java.awt.geom.*;

import javax.swing.*;

public class YToggleButton extends JToggleButton 
{
	
	Color 	_color;
	Color 	_pressedcolor;
	String 	_text;
	String 	_Pressedtext;
	Shape 	_shape;
	int 	_edgeRound;
	
	
	Boolean _LeftUpperRound;
	Boolean _RightUpperRound;
	Boolean _RightLowerRound;
	Boolean _LeftLowerRound;
	
	public YToggleButton() 
	{
		this(10);
	}
	
	public YToggleButton(String Text) 
	{
		this(Text, 10, true, true, true, true);
	}
	
	public YToggleButton(int EdgeRound) 
	{
		  
		this(EdgeRound, true, true, true, true);
	}
	
	public YToggleButton(int EdgeRound, Boolean LeftUpperRound, Boolean RightUpperRound, Boolean RightLowerRound ,Boolean LeftLowerRound) 
    {
		this("YToggleButton",EdgeRound, true, true, true, true);
    }
	
    public YToggleButton(String Text, int EdgeRound, Boolean LeftUpperRound, Boolean RightUpperRound, Boolean RightLowerRound ,Boolean LeftLowerRound) 
    {
		_text = Text;
	    _edgeRound = EdgeRound;
	    _Pressedtext = null;
	    
	    _LeftUpperRound  = LeftUpperRound;
	    _RightUpperRound = RightUpperRound;
	    _RightLowerRound = RightLowerRound;
	    _LeftLowerRound  = LeftLowerRound;
	    
	    _color = YTheme.getColor(YTheme.C_OBJECT_BACKGROUND);
	    _pressedcolor = YTheme.getColor(YTheme.C_PRIMARY);
	    
	    Dimension size = getPreferredSize();
	    size.width = size.height = Math.max(size.width, size.height);
	    
	    setForeground(YTheme.getColor(YTheme.C_OBJECT_FOREGROUND));
	    setFont(YTheme.F_REGULAR(12));
	    setFocusPainted(false);
	    setBorderPainted(false);
	    setContentAreaFilled(false);
	    setPreferredSize(size);
	    setMargin(new Insets(2, 2, 2, 2));
	  
    }
  
  
   
  
  protected void paintComponent(Graphics g) 
  {
	  Graphics2D g2d = (Graphics2D) g.create();
	  g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	   
	   
	  if (getModel().isSelected()) 
	  {
		  g2d.setColor(_pressedcolor);
		  
		  if(_Pressedtext != null)
		  {
			  super.setText(_Pressedtext);
		  }
		  else
		  {
			  super.setText(_text);
		  }
	  } 
	  else 
	  {
		  g2d.setColor(_color);
		  super.setText(_text);
	  }
   

	  g2d.fill(_shape = CustomArea());
	  g2d.dispose();
    
	  super.paintComponent(g);
    
	  g2d.dispose();
  }


  public boolean contains(int x, int y) 
  {
	  
    if (_shape == null || !_shape.getBounds().equals(getBounds())) 
    {
    	_shape = CustomArea();
    }
    return _shape.contains(x, y);
  }
  
  
  Area CustomArea() 
  {
	  
	  Area area = new Area(new RoundRectangle2D.Float(0, 0, getWidth(), getHeight(), _edgeRound, _edgeRound));
	  
	  
	  if(!_LeftUpperRound)
	  {
		  area.add(new Area(new Rectangle(0, 0, getWidth()/2, getHeight()/2)));
	  }
	  if(!_RightUpperRound)
	  {
		  area.add(new Area(new Rectangle(getWidth()/2 + 1, 0, getWidth()/2, getHeight()/2)));
	  }
	  if(!_RightLowerRound)
	  {
		  area.add(new Area(new Rectangle(getWidth()/2 + 1, getHeight()/2 + 1, getWidth()/2, getHeight()/2)));
	  }
	  if(!_LeftLowerRound)
	  {
		  area.add(new Area(new Rectangle(0, getHeight()/2 + 1, getWidth()/2, getHeight()/2)));
	  }
	  
	  
	return area; 
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  public void setRoundEdges(Boolean LeftUpperRound, Boolean RightUpperRound, Boolean RightLowerRound ,Boolean LeftLowerRound)
  {
  	_LeftUpperRound  = LeftUpperRound;
	    _RightUpperRound = RightUpperRound;
	    _RightLowerRound = RightLowerRound;
	    _LeftLowerRound  = LeftLowerRound;
  }

  public void setEdgeRoundness(int edgeRound)
  {
  	_edgeRound = edgeRound;
  }
  
  @Override public void setBackground(Color color)
  {
  	_color = color;
  }
  
  @Override public void setText(String Text)
  {
	  _text = ("<html>" + Text + "</html>");
  }
  
  public void setPressedText(String Text)
  {
	 _Pressedtext = ("<html>" + Text + "</html>");
  }
  
  public void setPressedBackground(Color color)
  {
  	_pressedcolor = color;
  }




























  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}