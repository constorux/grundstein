package YComponentsLegacy;

import YComponents.YLabel;
import YComponents.YPanel;
import YComponents.YTheme;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class YMessage extends JPanel
{
	YLabel _lbl_Message_Body;
	Color 	_color;
	int 	_width;
	YPanel _pan_Base;
	
	public YMessage(int width ,boolean own, String Username, String Time)
	{
		this(width,own,Username,Time,"");
	}
	
	public YMessage(int width ,boolean own, String Username, String Time, String Text) 
	{	  
		int edgeRound = 20;
		_width = width;
		_color = YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_DARK_2);
	    
	    Boolean LeftUpperRound  = true;
	    Boolean RightUpperRound = true;
	    Boolean RightLowerRound = true;
	    Boolean LeftLowerRound  = false;
	   
	    setOpaque(false);
	    super.setBackground(new Color(0,0,0,0));
		setLayout(new BorderLayout(0, 0));
		setBorder(new EmptyBorder(5, 5, 5, 15));
		
		if(own == true)
	    {
	    	
	    	RightLowerRound = false;
	    	LeftLowerRound  = true;
	    	
	    	setBorder(new EmptyBorder(5, 15, 5, 5));
	    	_color = YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_DARK_3);
	    }
		
		
		
		
		_pan_Base = new YPanel();
		_pan_Base.setOpaque(true);
		_pan_Base.setBackground(_color);
		_pan_Base.setLayout(new BorderLayout(0, 0));
		_pan_Base.setRoundEdges(LeftUpperRound, RightUpperRound, RightLowerRound, LeftLowerRound);
		_pan_Base.setEdgeRoundness(edgeRound);
	  	add(_pan_Base, BorderLayout.CENTER);
		
		JPanel pan_Message_Header = new JPanel();
		pan_Message_Header.setPreferredSize(new Dimension(10, 20));
		pan_Message_Header.setBorder(new EmptyBorder(3, 5, 2, 5));
		_pan_Base.add(pan_Message_Header, BorderLayout.NORTH);
		pan_Message_Header.setLayout(new BorderLayout(0, 0));
		pan_Message_Header.setOpaque(false);
		
		YLabel lbl_UserName = new YLabel("");
		lbl_UserName.setTextNoHTML(Username);
		pan_Message_Header.add(lbl_UserName, BorderLayout.WEST);
		lbl_UserName.setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_1));
		
		YLabel lbl_TimeStamp = new YLabel("");
		lbl_TimeStamp.setHorizontalTextPosition(SwingConstants.RIGHT);
		lbl_TimeStamp.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_TimeStamp.setTextNoHTML(Time);
		pan_Message_Header.add(lbl_TimeStamp, BorderLayout.EAST);
		lbl_TimeStamp.setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_1));
		
		_lbl_Message_Body = new YLabel("working...");
		_lbl_Message_Body.setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_1));
		_lbl_Message_Body.setWidth((_width - 26));
		_lbl_Message_Body.setBorder(new EmptyBorder(3, 3, 3, 3));
		_pan_Base.add(_lbl_Message_Body, BorderLayout.CENTER);
	  
		
		setText(Text);
    }
  
  
    
  

  public void setBackground(Color color)
  {
  		_color = color;
  }

  public void setText (String text)
  {
	  _lbl_Message_Body.setTextWidthSpecific(text);
	  _pan_Base.add(_lbl_Message_Body, BorderLayout.CENTER);
	  resizeComponent((int) _lbl_Message_Body.getPreferredSize().getHeight() + 40);
	  
  }
  
  public void setFile (String FileID, String Filename, ImageIcon Icon)
  {
	  _lbl_Message_Body.setVisible(false);
	  YFilePreview _yfp_FilePreview= new YFilePreview(FileID, Filename, Icon);
	  _pan_Base.add(_yfp_FilePreview, BorderLayout.CENTER);
	  
	  resizeComponent((int) _yfp_FilePreview.getPreferredSize().getHeight() + 40);
  }

  void resizeComponent (int y)
  {
	
	  setMinimumSize(new Dimension(_width, y));
	  setPreferredSize(new Dimension(_width, y));
	  setMaximumSize(new Dimension(_width, y));
	
	  this.revalidate();
  }


  




  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}