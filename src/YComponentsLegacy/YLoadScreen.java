package YComponentsLegacy;

import YComponents.YLabel;
import YComponents.YTheme;

import javax.swing.*;


public class YLoadScreen extends JDialog
{
	YLabel _ylbl_Status;
	
	public YLoadScreen(String Edition ,String Version, ImageIcon icon, ImageIcon Background)
	{
		
		setSize(300, 200);
		setResizable(false);
		setAlwaysOnTop(true);
		setUndecorated(true);
		setLocationRelativeTo(null);
		
		
		
		JPanel pan_Base = new JPanel();
		pan_Base.setBounds(0, 0, 300, 200);
		getContentPane().add(pan_Base);
		pan_Base.setLayout(null);
		
		YLabel ylbl_Background = new YLabel();
		ylbl_Background.setBounds(0, 0, 300, 200);
		pan_Base.add(ylbl_Background);
		ylbl_Background.setIcon(Background);
		
		JPanel pan_Cotrols = new JPanel();
		pan_Cotrols.setOpaque(false);
		pan_Cotrols.setBounds(0, 0, 300, 200);
		ylbl_Background.add(pan_Cotrols);
		pan_Cotrols.setLayout(null);
		
		YLabel ylbl_Logo = new YLabel();
		ylbl_Logo.setHorizontalAlignment(SwingConstants.LEFT);
		ylbl_Logo.setVerticalAlignment(SwingConstants.TOP);
		ylbl_Logo.setBounds(10, 10, 210, 120);
		pan_Cotrols.add(ylbl_Logo);
		ylbl_Logo.setIcon(icon);
		
		YLabel ylbl_Version = new YLabel();
		ylbl_Version.setBounds(80, 130, 200, 20);
		pan_Cotrols.add(ylbl_Version);
		ylbl_Version.setHorizontalAlignment(SwingConstants.RIGHT);
		ylbl_Version.setText(Version);
		
		YLabel ylbl_Edition = new YLabel();
		ylbl_Edition.setBounds(80, 115, 200, 20);
		pan_Cotrols.add(ylbl_Edition);
		ylbl_Edition.setFont(YTheme.F_BOLD(12));
		ylbl_Edition.setText(Edition);
		ylbl_Edition.setHorizontalAlignment(SwingConstants.RIGHT);
		
		_ylbl_Status = new YLabel();
		_ylbl_Status.setText("loading...");
		_ylbl_Status.setHorizontalAlignment(SwingConstants.RIGHT);
		_ylbl_Status.setBounds(80, 170, 200, 20);
		pan_Cotrols.add(_ylbl_Status);
		
		setVisible(true);
	}
	
	public void setText(String Text)
	{
		_ylbl_Status.setText(Text);
	}
	
	public void Done()
	{
		setVisible(false);
	}
	
}

  


  
  