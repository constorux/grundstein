package YComponentsLegacy;

import YComponents.YButton;
import YComponents.YLabel;
import YComponents.YTheme;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class YContentPane extends JPanel
{
	JPanel _pan_Footer;
	public JPanel _pan_Content;
	Boolean _showFooter;
	JPanel _pan_FooterBase;
	String _ID;
	String _title;
	
	JPanel _pan_Header;
	YLabel _ylbl_PageTitle;
	
	public YContentPane() 
	{	  
		this(false, "unnamed pane", "");
    }
	
	public YContentPane(Boolean showFooter) 
	{	  
		this(showFooter, "unnamed pane", "");
    }
	
	public YContentPane(String name) 
	{	  
		this(false, name, "unnamed pane");
    }
	
	public YContentPane(String ID, String title) 
	{	  
		this(false, ID, title);
    }
	
	public YContentPane(Boolean showFooter,String ID, String title) 
	{	  
		
		
		
		_showFooter = showFooter;
		_title = title;
		_ID = ID;
		super.setLayout(new BorderLayout(0, 0));
		
		JPanel ContentBase = new JPanel();
		ContentBase.setOpaque(false);
		ContentBase.setLayout(new BorderLayout(0, 0));
		add(ContentBase, BorderLayout.CENTER);
		
		_pan_Content = new JPanel();
		_pan_Content.setOpaque(false);
		_pan_Content.setLayout(new BoxLayout(_pan_Content, BoxLayout.Y_AXIS));
		_pan_Content.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_LIGHT_8));
		ContentBase.add(_pan_Content, BorderLayout.CENTER);
		
		_pan_FooterBase = new JPanel();
		_pan_FooterBase.setPreferredSize(new Dimension(40, 40));
		_pan_FooterBase.setMaximumSize(new Dimension(30000, 40));
		_pan_FooterBase.setBackground(YTheme.getColor(YTheme.C_CONTROL));
		add(_pan_FooterBase, BorderLayout.SOUTH);
		_pan_FooterBase.setLayout(new BorderLayout(0, 0));
		_pan_FooterBase.setVisible(_showFooter);
		
		_pan_Footer = new JPanel();
  		_pan_Footer.setPreferredSize(new Dimension(10, 40));
  		_pan_Footer.setOpaque(false);
  		_pan_FooterBase.add(_pan_Footer, BorderLayout.CENTER);
  		_pan_Footer.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
  		
  		_pan_Header = new JPanel();
  		_pan_Header.setPreferredSize(new Dimension(40, 40));
  		_pan_Header.setBackground(YTheme.getColor(YTheme.C_CONTROL));
  		add(_pan_Header, BorderLayout.NORTH);
  		_pan_Header.setLayout(new BorderLayout(0, 0));
  		_pan_Header.setVisible(false);
  		
  		_ylbl_PageTitle = new YLabel("");
  		_ylbl_PageTitle.setFont(YTheme.F_REGULAR(18));
  		_ylbl_PageTitle.setForeground(YTheme.getColor(YTheme.C_TEXT_DARK_3));
  		_ylbl_PageTitle.setPreferredSize(new Dimension(400, 40));
  		_ylbl_PageTitle.setBorder(new EmptyBorder(7, 7, 7, 7));
  		//ylbl_PageTitle.setBackground(YTheme.C_ACCENT);
  		_pan_Header.add(_ylbl_PageTitle, BorderLayout.WEST);
  		
  		
  		
  		//setting it up
  		if((title != null)&&(!title.equals("")))
		{
			setHeaderTitle(title);
		}
    }
  
 
  
  @Override public Component add(Component c)
  {
	  
  		return _pan_Content.add(c);
  }
  
//  @Override public void setLayout(LayoutManager mgr)
//  {
//	  setContentLayout(mgr);
//  }
  
  public void setContentLayout(LayoutManager mgr)
  {
  		_pan_Content.setLayout(mgr);
  }
  
  public void resetContentLayout()
  {
  		_pan_Content.setLayout(null);
  }
  
  
 
  public YButton addFooterButton()
  {
	  return addFooterButton("YButton");
  }
  
  public YButton addFooterButton(YButton footerButton)
  {
	  _showFooter = true;
	  _pan_FooterBase.setVisible(_showFooter);
	  footerButton.setPreferredSize(new Dimension(footerButton.getPreferredSize().width, 30));
	  _pan_Footer.add(footerButton);
		 
		 return footerButton;
  }
  
  public YButton addFooterButton(String Text)
  {
	 
	  
	 YButton btn_HeaderControl = new YButton(Text);
	 btn_HeaderControl.setPreferredSize(new Dimension(100, 30));
	 btn_HeaderControl.setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_1));
	 btn_HeaderControl.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_LIGHT_1));
	 _pan_Footer.add(btn_HeaderControl);
	 
	 return addFooterButton(btn_HeaderControl);
  }
  
  public YToggleButton addFooterToggleButton()
  {
	 _showFooter = true;
	 _pan_FooterBase.setVisible(_showFooter);
	  
	 YToggleButton btn_HeaderControl = new YToggleButton();
	 btn_HeaderControl.setPreferredSize(new Dimension(100, 30));
	 btn_HeaderControl.setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_1));
	 btn_HeaderControl.setBackground(YTheme.getColor(YTheme.C_TEXT_LIGHT_5));
	 btn_HeaderControl.setPressedBackground(YTheme.getColor(YTheme.C_PRIMARY));
	 _pan_Footer.add(btn_HeaderControl);
	 
	 return btn_HeaderControl;
  }

  public void showFooter(boolean showFooter)
  {
	 _showFooter = showFooter;
	 _pan_FooterBase.setVisible(_showFooter);
  }
  
  public void showHeader(boolean showFooter)
  {
	  _pan_Header.setVisible(_showFooter);
  }

  @Override public void remove(Component comp)
  {	
	  _pan_Content.remove(comp);
  }
  
  @Override public void removeAll()
  {	
	  _pan_Content.removeAll();
  }

  	public String getPaneID()
  	{
  		return _ID;
  	}
  	
  	public String getPaneTitle()
  	{
  		return _title;
  	}

  	void setHeaderTitle(String title)
    {
  		_ylbl_PageTitle.setText(title);
  		_pan_Header.setVisible(true);
    }




  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}