package YComponentsLegacy;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import YComponents.YButton;
import YComponents.YLabel;
import YComponents.YTheme;

public class YBase extends JFrame 
{
	JPanel _pan_MenuItems;
	JPanel _pan_Header_Controls;
	JPanel _pan_Content;
	JPanel _pan_Content_2;
	YLabel _lbl_MenuHeader_Title;
	JPanel _pan_Header_controls;
	JPanel _pan_Chat;
	JPanel _pan_Chat_Menu;
	JPanel _pan_Header;

	ArrayList<YContentPane> _childComponents = new ArrayList<YContentPane>();
	ArrayList<String> _childComponentNames = new ArrayList<String>();
	
	YContentPane _ycp_loadingPage;
	YLabel _ylbl_PageTitle;
	YButton _ybtn_closeApplication;
	YMenuItem _pan_MenuFooter;
	JPanel _pan_MenuBase;
	
	int threadYPos;
	int threadXPos;
	JPanel threadTopContent;
	
	public static int ANIMATION_UP    = 0;
	public static int ANIMATION_DOWN  = 1;
	public static int ANIMATION_LEFT  = 2;
	public static int ANIMATION_RIGHT = 3;
	public static int ANIMATION_NONE = -1;

	//fullscreen
	static GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0];
	
  public YBase() 
  {
	  this("Yourcore Application");
  }
	
  public YBase(String Title) 
  {
	  
	Dimension size = getPreferredSize();
//   size.width = 180; 
//   size.height = 40;
    
    setLayout(new BorderLayout(0, 0));
    
    //DEBUG
    Toolkit temp = Toolkit.getDefaultToolkit();
    Dimension d = temp.getScreenSize();
    Image i = temp.getImage(YBase.class.getResource("/YComponents/resources/yc_logo.png"));
    setIconImage(i);
    
    //setIconImage(Toolkit.getDefaultToolkit().getImage(YBase.class.getResource("/YComponents/resources/yc_logo.png")));
    setDefaultCloseOperation(HIDE_ON_CLOSE);
	setBounds(100, 100, 850, 600);
	setMinimumSize(new Dimension(400,250));
	setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    
	_pan_MenuBase = new JPanel();
	_pan_MenuBase.setPreferredSize(new Dimension(40, 10));
	add(_pan_MenuBase, BorderLayout.WEST);
	_pan_MenuBase.setLayout(new BorderLayout(0, 0));
	
	JPanel pan_MenuHeader = new JPanel();
	pan_MenuHeader.setPreferredSize(new Dimension(180, 40));
	pan_MenuHeader.setBackground(YTheme.getColor(YTheme.C_MENU_HEADER));
	pan_MenuHeader.setMinimumSize(new Dimension(180,10));
	_pan_MenuBase.add(pan_MenuHeader, BorderLayout.NORTH);
	pan_MenuHeader.setLayout(new BorderLayout(0, 0));
	
	_lbl_MenuHeader_Title = new YLabel();
	_lbl_MenuHeader_Title.setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_1));
	_lbl_MenuHeader_Title.setBorder(new EmptyBorder(10, 10, 10, 10));
	pan_MenuHeader.add(_lbl_MenuHeader_Title, BorderLayout.CENTER);
	
	YMenuButton btn_MenuHeader_Btn = new YMenuButton();
	btn_MenuHeader_Btn.setPreferredSize(new Dimension(40, 40));
	pan_MenuHeader.add(btn_MenuHeader_Btn, BorderLayout.EAST);
	
	JPanel pan_Menu_Body = new JPanel();
	pan_Menu_Body.setBackground(YTheme.getColor(YTheme.C_MENU_BODY));
	_pan_MenuBase.add(pan_Menu_Body, BorderLayout.CENTER);
	pan_Menu_Body.setLayout(new BorderLayout(0, 0));
	
	JPanel pan_Menu_Body_Seperator = new JPanel();
	pan_Menu_Body_Seperator.setOpaque(false);
	pan_Menu_Body_Seperator.setPreferredSize(new Dimension(10, 3));
	pan_Menu_Body_Seperator.setBackground(YTheme.getColor(YTheme.C_PRIMARY));
	pan_Menu_Body.add(pan_Menu_Body_Seperator, BorderLayout.NORTH);
	
	_pan_MenuItems = new JPanel();
	_pan_MenuItems.setOpaque(false);
	pan_Menu_Body.add(_pan_MenuItems, BorderLayout.CENTER);
	_pan_MenuItems.setLayout(new FlowLayout(FlowLayout.RIGHT, 0, 2));
	
	
	
	//YMenuItem pan_MenuFooter = new YMenuItem("BETA", new ImageIcon( YBase.class.getResource("/YComponents/resources/beta-icon.png")));
	
	_pan_MenuFooter = new YMenuItem(null, "BETA", new ImageIcon( YBase.class.getResource("/YComponents/resources/beta-icon.png")));
	_pan_MenuFooter.setColor(YTheme.getColor(YTheme.C_ACCENT));
	_pan_MenuBase.add(_pan_MenuFooter, BorderLayout.SOUTH);
	_pan_MenuFooter.setVisible(false);
	
	
	
	JPanel pan_Content_Base = new JPanel();
	add(pan_Content_Base, BorderLayout.CENTER);
	pan_Content_Base.setLayout(new BorderLayout(0, 0));
	
	_pan_Chat = new JPanel();
	//_pan_Chat.setPreferredSize(new Dimension(40, 40));
	_pan_Chat.setBackground(YTheme.getColor(YTheme.C_ACCENT));
	 add(_pan_Chat, BorderLayout.EAST);
	_pan_Chat.setLayout(new BorderLayout(0, 0));
	_pan_Chat.setVisible(false);
	
	_pan_Content = new JPanel();
	pan_Content_Base.add(_pan_Content, BorderLayout.CENTER);
	_pan_Content.setLayout(new BorderLayout(0, 0));
	
	//_pan_Content_2 = new JPanel();
	//pan_Content_Base.add(_pan_Content_2, BorderLayout.CENTER);
	//_pan_Content_2.setLayout(new BorderLayout(0, 0));
	
	_pan_Header = new JPanel();
	_pan_Header.setPreferredSize(new Dimension(40, 40));
	_pan_Header.setBackground(YTheme.getColor(YTheme.C_CONTROL));
	pan_Content_Base.add(_pan_Header, BorderLayout.NORTH);
	_pan_Header.setLayout(new BorderLayout(0, 0));
	_pan_Header.setVisible(false);
	
	_ylbl_PageTitle = new YLabel("");
	_ylbl_PageTitle.setFont(YTheme.F_REGULAR(18));
	_ylbl_PageTitle.setForeground(YTheme.getColor(YTheme.C_TEXT_DARK_3));
	_ylbl_PageTitle.setPreferredSize(new Dimension(400, 40));
	_ylbl_PageTitle.setBorder(new EmptyBorder(7, 7, 7, 7));
	//ylbl_PageTitle.setBackground(YTheme.C_ACCENT);
	_pan_Header.add(_ylbl_PageTitle, BorderLayout.WEST);
	
	
	_pan_Header_controls = new JPanel();
	_pan_Header_controls.setPreferredSize(new Dimension(10, 40));
	_pan_Header_controls.setOpaque(false);
	_pan_Header.add(_pan_Header_controls, BorderLayout.CENTER);
	_pan_Header_controls.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
	
	_ybtn_closeApplication = new YButton("x");
	_ybtn_closeApplication.setBackground(YTheme.getColor(YTheme.C_PRIMARY));
	_ybtn_closeApplication.setBorder(new EmptyBorder(5, 5, 5, 5));
	_ybtn_closeApplication.setEdgeRoundness(30);
	_ybtn_closeApplication.setPreferredSize(new Dimension(40,40));
	_ybtn_closeApplication.setRoundEdges(false, false, false, true);
	_ybtn_closeApplication.setVisible(false);
	_pan_Header.add(_ybtn_closeApplication, BorderLayout.EAST);
	
	
	//TODO loading page
	
	_ycp_loadingPage = new YContentPane(true, "loading", "");
	_ycp_loadingPage.setBounds(12, 12, 755, 421);
	_ycp_loadingPage.setOpaque(true);
	addContent(_ycp_loadingPage, false);
	_ycp_loadingPage.setContentLayout(new BorderLayout(0, 0));
	
	YLabel ylbl_loadingText = new YLabel("[no view selected]");
	ylbl_loadingText.setFont(YTheme.F_BOLD(35));
	ylbl_loadingText.setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_5));
	ylbl_loadingText.setHorizontalAlignment(SwingConstants.CENTER);
	_ycp_loadingPage.add(ylbl_loadingText, SwingConstants.CENTER);
	
	
	//start
	showLoadingPage();
	setTitle(Title);
	
	
	
	
	
	//action listeners
	
	JFrame _this = this;
	_ybtn_closeApplication.addActionListener( new ActionListener()
	{
		public void actionPerformed(ActionEvent e) 
		{
			dispatchEvent(new WindowEvent(_this, WindowEvent.WINDOW_CLOSING));
		}
	});
	
	btn_MenuHeader_Btn.addActionListener( new ActionListener()
	{
	   
		public void actionPerformed(ActionEvent e) 
		{
		
			Boolean OpenMenu;
			
			if(_pan_MenuBase.getWidth() >= 180)
			{
				OpenMenu = false;
			}
			else 
			{
				OpenMenu = true;
			}
			
			Timer timer = new Timer(10, new ActionListener() 
			{
			    @Override
			    public void actionPerformed(ActionEvent ae) 
			    {
			    	if(OpenMenu)
			    	{
			    		
			    		_pan_MenuBase.setPreferredSize(new Dimension(((int) (_pan_MenuBase.getWidth()) + 4),10));
			    		getContentPane().revalidate();
			    	}
			    	else
			    	{
			    		_lbl_MenuHeader_Title.setVisible(false);
			    		
			    		if(_pan_MenuBase.getWidth() < 50)
			    		{
			    			_pan_MenuBase.setPreferredSize(new Dimension(((int) (_pan_MenuBase.getWidth()) - 2),10));
			    		}
			    		else
			    		{
			    			_pan_MenuBase.setPreferredSize(new Dimension(((int) (_pan_MenuBase.getWidth()) - 10),10));
			    		}
			    		
			    		getContentPane().revalidate();
			    	}
		 	
		    	    if (_pan_MenuBase.getWidth() > 180)
		    	    {
		    	    	_lbl_MenuHeader_Title.setVisible(true);
		    	    	_pan_MenuBase.setPreferredSize(new Dimension(180, 10));
		    	    	((Timer)ae.getSource()).stop();
		    	    }
		    	    else if (_pan_MenuBase.getWidth() < 40)
		    	    {
		    	    	_lbl_MenuHeader_Title.setVisible(false);
		    	    	_pan_MenuBase.setPreferredSize(new Dimension(40, 10));
		    	    	((Timer)ae.getSource()).stop();
		    	    }
			    }
			});
			
			timer.start(); 
			
		}
	});
	
	
	
  } 
  public void addMenuItem(YMenuItem MenuItem)
  {
	   _pan_MenuItems.add(MenuItem);
	   revalidate();
	   
	   MenuItem.addActionListener( new ActionListener()
		{ 
			public void actionPerformed(ActionEvent e) 
			{
				
				showContent(MenuItem.getID());
				
			}
		});
  }
  
//  public YButton addHeaderBackButton()
//  {
//	  	YButton ybtn_HeaderBack = new YButton(30, false, true, true, false);
//	  	ybtn_HeaderBack.setPreferredSize(new Dimension(40, 40));
//	  	ybtn_HeaderBack.setBorder(new EmptyBorder(7, 7, 7, 7));
//	  	ybtn_HeaderBack.setBackground(YTheme.C_ACCENT);
//	  	ybtn_HeaderBack.setIcon(new ImageIcon( YBase.class.getResource("/YComponents/resources/yc_button_back.png")), 0.7);
//		_pan_Header.add(ybtn_HeaderBack, BorderLayout.WEST);
//		
//		return (ybtn_HeaderBack);
//  }
  
  public void clearHeaderControls()
  {
	  _pan_Header_controls.removeAll();
	  repaint();
  }
  
  public void clearMenuItems()
  {
	  _pan_MenuItems.removeAll();
	  repaint();
  }
  
  
  
 
  public YButton addHeaderButton()
  {
	  return addHeaderButton("YButton");
  }
  
  public YButton addHeaderButton(String Text)
  {
	 YButton btn_HeaderControl = new YButton(Text);
	 btn_HeaderControl.setPreferredSize(new Dimension(100, 30));
	 btn_HeaderControl.setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_1));
	 btn_HeaderControl.setBackground(YTheme.getColor(YTheme.C_ACCENT));
	 _pan_Header_controls.add(btn_HeaderControl);
	 
	 return btn_HeaderControl;
  }
  
  public YButton addHeaderButton(YButton b)
  {
	  b.setPreferredSize(new Dimension(b.getPreferredSize().width, 30));
	  _pan_Header_controls.add(b);
		 
		 return b;
  }
  
  public void addContent(YContentPane Item)
  {
	  addContent(Item,true);
  }
  
  public void addContent(YContentPane Item, boolean addToMenu)
  {
	  addContent(Item,addToMenu, null);
  }
  
  public void addContent(YContentPane Item, boolean addToMenu, ImageIcon menuIcon)
  {
	  if(Item != null)
	  {
		 _childComponents.add(Item);
		  _childComponentNames.add(Item.getPaneID());
		  // _pan_Content.add(Item, Name);
		  if(addToMenu)
		  {
			  addMenuItem(new YMenuItem(Item, menuIcon));
		  }
	  }
	  
	  
  }
  
  @Override public Component add(Component Item)
  {
	  if(Item.getClass() == YContentPane.class)
	  {
		  addContent(((YContentPane) Item));
	  }
	  else
	  {
		  System.out.println("component type not supported");
	  }
	  	
	    //addMenuItem(new YMenuItem(paneName));
	    
	    return Item;
  }
  
  public void isBETA(boolean IsBeta)
  {
	 _pan_MenuFooter.setVisible(IsBeta);
  }
  
  public void showContent(String Name)
  {
	  showContent(Name, ANIMATION_LEFT);
  }
  
  public void showContent(String Name, int Animation)
  {
	 if(Animation == 0)
	 {
		 threadYPos = 20;
		 threadXPos = 0;
	 }
	 else if(Animation == 1)
	 {
		 threadYPos = -20;
		 threadXPos = 0;
	 }
	 else if(Animation == 2)
	 {
		 threadYPos = 0;
		 threadXPos = -20;
	 }
	 else if(Animation == 3)
	 {
		 threadYPos = 0;
		 threadXPos = 20;
	 }
	 else
	 {
		 threadYPos = 0;
		 threadXPos = 0;
	 }
	  
	  for(int i = 0;  i <_childComponentNames.size() ; i++)
	  {
		  if(_childComponentNames.get(i).equals(Name))
		  {
			  //System.out.println(Name);
			  YContentPane pane = _childComponents.get(i);
			  pane.setLocation(0,threadYPos);
			  _pan_Content.removeAll();
			  _pan_Content.add(pane);
			  revalidate();
			  
			  Timer timer = new Timer(10, new ActionListener() 
			  {
				    @Override
				    public void actionPerformed(ActionEvent ae) 
				    {
						if ((threadYPos == 0)&& (threadXPos == 0))
				        {
				        	_pan_Content.setBackground(new Color(255,255,255));
				        	repaint();
				        	revalidate();
				        	((Timer)ae.getSource()).stop();
				        }
				        else
				        {
				        	_pan_Content.setBackground(new Color(95 + (Math.abs(threadYPos + threadXPos) * 8),95 + (Math.abs(threadYPos + threadXPos) * 8),95 + (Math.abs(threadYPos + threadXPos) * 8)));
				        	//_pan_Content.setBackground(new Color(15 + (threadYPos * 12),15 + (threadYPos * 12),15 + (threadYPos * 12)));
							pane.setLocation(threadXPos,threadYPos);
							repaint();
						}
						if(threadYPos == 0)
						{
							
						}
						else if(threadYPos > 0)
						{
							threadYPos--;
						}
						else
						{
							threadYPos++;
						}
						
						if(threadXPos == 0)
						{
							
						}
						else if(threadXPos > 0)
						{
							threadXPos--;
						}
						else
						{
							threadXPos++;
						}
		            }
		        });
			  	timer.start();
		  return;
		  }
	  }
	  System.out.println("component was not found");
  }
  
  public void showLoadingPage()
  {
	showContent("loading");
  }
  
  public void showMenu(boolean show)
  {
	_pan_MenuBase.setVisible(show);
  }
  
  public void showHeader(boolean show)
  {
	  _pan_Header.setVisible(show);
  }
  
  
  public void setFullscreen(boolean fullscreen)
  {
	  _ybtn_closeApplication.setVisible(fullscreen);  
	  
	if(fullscreen)
	{
		 device.setFullScreenWindow(this);
	}
	else
	{
		 device.setFullScreenWindow(null);
	}	
  }
  
  
  
  //internal voids
  void setHeaderTitle(String title)
  {
	 _ylbl_PageTitle.setText(title);
	 showHeader(true);
  }
  
  public void setTitle(String Title)
  {
	  _lbl_MenuHeader_Title.setText(Title);
	  super.setTitle(Title);
  }
  
  
  
  
  
  
  
  
  
  
}