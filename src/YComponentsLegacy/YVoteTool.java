package YComponentsLegacy;

import YComponents.YLabel;
import YComponents.YPanel;
import YComponents.YTheme;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.EmptyBorder;

public class YVoteTool extends YPanel
{
	
	int _count;
	YLabel lbl_Count;
	YToggleButton _btn_UpVote;
	YToggleButton _btn_DownVote;
	
  public YVoteTool(int VoteCount) 
  {
	_count = VoteCount;  
	
	  
	Dimension size = getPreferredSize();
	
    setBounds(463, 319, 70, 20);
	setPreferredSize(new Dimension(70, 10));
	setRoundEdges(true, true, true, true);
	setEdgeRoundness(40);
	setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_LIGHT_4));
	setLayout(new BorderLayout(0, 0));
	
	_btn_UpVote = new YToggleButton(30, true, true, false, true);
	_btn_UpVote.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_LIGHT_3));
	_btn_UpVote.setPreferredSize(new Dimension(size.height, 10));
	add(_btn_UpVote, BorderLayout.WEST);
	_btn_UpVote.setText("+");
	_btn_UpVote.setPressedBackground(YTheme.getColor(YTheme.C_GREEN));
	
	_btn_DownVote = new YToggleButton(30, true, true, true, false);
	_btn_DownVote.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_LIGHT_3));
	_btn_DownVote.setPreferredSize(new Dimension(size.height, 10));
	add(_btn_DownVote, BorderLayout.EAST);
	_btn_DownVote.setText("");
	_btn_DownVote.setPressedBackground(YTheme.getColor(YTheme.C_RED));
	
	lbl_Count = new YLabel();
	lbl_Count.setText("");
	lbl_Count.setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_1));
	lbl_Count.setBorder(new EmptyBorder(10, 10, 10, 10));
	add(lbl_Count, BorderLayout.CENTER);
	
	_btn_UpVote.addActionListener(new ActionListener() 
	{
		public void actionPerformed(ActionEvent e) 
		{
			_count++;
			refresh();
			_btn_UpVote.setEnabled(false);
			_btn_DownVote.setEnabled(false);
			
		}
	});
	
	_btn_DownVote.addActionListener(new ActionListener() 
	{
		public void actionPerformed(ActionEvent e) 
		{
			_count--;
			refresh();
			_btn_UpVote.setEnabled(false);
			_btn_DownVote.setEnabled(false);
		}
	});
	
  
  } 
  
  public int getVoteCount()
  {
  		return _count;
  }
  
  public void setVoteCount(int VoteCount)
  {
  		_count = VoteCount;
  }
  
  public void refresh()
  {
	  Dimension size = getSize();
	  lbl_Count.setText(Integer.toString(_count));
	  _btn_DownVote.setPreferredSize(new Dimension(size.height, 10));
	  _btn_UpVote.setPreferredSize(new Dimension(size.height, 10));
  }
  
  
}