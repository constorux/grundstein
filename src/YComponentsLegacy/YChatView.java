package YComponentsLegacy;

import YComponents.YButton;
import YComponents.YLabel;
import YComponents.YTheme;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class YChatView extends JPanel
{
	Color 	_color;
	Container _comp_MainArea_Items;
	YScrollPane _yscp_MainArea;
	YLabel _ylbl_ChatName;
	JPanel _pan_Controls;
	JPanel pan_ChatMenu;
	
	public YChat chat;
	public YChatView() 
	{	  
		setLayout(new BorderLayout(0, 0));
		setBackground(YTheme.getColor(YTheme.C_PRIMARY));
		
		chat = new YChat();
		add(chat, BorderLayout.CENTER);
		chat.setPreferredSize(new Dimension(300, 10));
		
		pan_ChatMenu = new JPanel();
		pan_ChatMenu.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_DARK_3));
		pan_ChatMenu.setPreferredSize(new Dimension(40, 40));
		add(pan_ChatMenu, BorderLayout.EAST);
		pan_ChatMenu.setLayout(new BorderLayout(0, 0));
		
		_pan_Controls = new JPanel();
		pan_ChatMenu.add(_pan_Controls, BorderLayout.CENTER);
		_pan_Controls.setOpaque(false);
		_pan_Controls.setPreferredSize(new Dimension(40, 40));
		_pan_Controls.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		
		YMenuButton btn_MenuHeader = new YMenuButton();
		btn_MenuHeader.setPreferredSize(new Dimension(40, 40));
		pan_ChatMenu.add(btn_MenuHeader, BorderLayout.NORTH);
		
		btn_MenuHeader.addActionListener( new ActionListener()
		{ 
			public void actionPerformed(ActionEvent e) 
			{
				chat.setVisible(false);
				setPreferredSize(new Dimension(40, 10));
			}
		});
		
		chat.setVisible(false);
		setPreferredSize(new Dimension(40, 10));
		revalidate();
    }
  
	
	
	public void OpenChat(String ChatTitle)
	  {
		
		chat.setVisible(true);
		chat.loadNewChat(ChatTitle);
		setPreferredSize(new Dimension((int)(40 + chat.getPreferredSize().getWidth()), 10));
	  }

	public void showMenu(boolean show)
	  {
		
		pan_ChatMenu.setVisible(show);
		
	  }

  
  
  public void addMenuButton(YButton button)
  {
	  
	  _pan_Controls.add(button);
	  
  }

 
  




  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}