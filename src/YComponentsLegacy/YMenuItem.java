package YComponentsLegacy;

import YComponents.YLabel;
import YComponents.YTheme;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;


public class YMenuItem extends JButton 
{
	String _ID;
	JPanel _pan_ItemBase;
	
//	public YMenuItem(String title, ImageIcon ItemIcon) 
//	{
//		 this(ID, ID, ItemIcon);
//	}
//	
	public YMenuItem(YContentPane p, ImageIcon itemIcon) 
	{
		 this(p.getPaneID(), p.getPaneTitle(), itemIcon);
	}
	
	public YMenuItem(String ID , String Title, ImageIcon itemIcon) 
	{
		_ID = ID;

		Dimension size = getPreferredSize();
		size.width = 180; 
		size.height = 40;
		
		if(itemIcon == null)
		{
			itemIcon = new ImageIcon(YMenuItem.class.getResource("/YComponents/resources/yb_send.png"));
		}
		
		setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_1));
		setLayout(new BorderLayout(0, 0));
		setPreferredSize(size);
		setOpaque(false);
		setMargin(new Insets(0,0,0,0));
		setBorder(null);
		setContentAreaFilled(false);
		
		
		_pan_ItemBase = new JPanel();
		_pan_ItemBase.setBackground(YTheme.getColor(YTheme.C_MENU_ITEM));
		add(_pan_ItemBase,  BorderLayout.CENTER);
		_pan_ItemBase.setBorder(new EmptyBorder(10, 10, 10, 10));
		_pan_ItemBase.setLayout(new BorderLayout(0, 0));
		  
	
		  YLabel lbl_MenuItem = new YLabel();
		lbl_MenuItem.setText(Title);
		lbl_MenuItem.setForeground(new Color(250, 250, 250));
		_pan_ItemBase.add(lbl_MenuItem, BorderLayout.CENTER);
	
	
		YLabel lbl_ItemIcon = new YLabel();
		lbl_ItemIcon.setPreferredSize(new Dimension(20,20));
		//lbl_ItemIcon.setVerticalAlignment(SwingConstants.CENTER);
		//lbl_ItemIcon.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_ItemIcon.setIcon(itemIcon);
		_pan_ItemBase.add(lbl_ItemIcon, BorderLayout.EAST);
	} 
	
	public String getID()
	{
		return _ID;
	}
	
	public void setColor(Color color)
	{
		_pan_ItemBase.setBackground(color);
	}
}