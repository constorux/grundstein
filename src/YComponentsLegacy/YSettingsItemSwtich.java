package YComponentsLegacy;

import YComponents.YLabel;
import YComponents.YSwitch;
import YComponents.YTheme;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class YSettingsItemSwtich extends JPanel 
{
	YSwitch _ysw_Switch;
	
  public YSettingsItemSwtich(String ItemName) 
  {
	
    setPreferredSize(new Dimension(550, 40));
    setMinimumSize(new Dimension(550, 40));
    setMaximumSize(new Dimension(550, 40));
	setBorder(new EmptyBorder(10, 10, 10, 10));
	setLayout(new BorderLayout(0, 0));
	setOpaque(false);
	
	YLabel lbl_Text = new YLabel();
	lbl_Text.setText(ItemName);
	lbl_Text.setForeground(YTheme.getColor(YTheme.C_TEXT_DARK_4));
	lbl_Text.setBorder(new EmptyBorder(10, 10, 10, 10));
	add(lbl_Text, BorderLayout.CENTER);
	
	_ysw_Switch = new YSwitch();
	_ysw_Switch.setPreferredSize(new Dimension(40, 10));
	_ysw_Switch.setBounds(697, 274, 180, 94);
	add(_ysw_Switch, BorderLayout.EAST);
	
  
  } 
  
  public YSwitch getSwitch()
  {
	  return _ysw_Switch;
  }
  
  public boolean isActive()
  {
	  return _ysw_Switch.isSelected();
  }
  
}