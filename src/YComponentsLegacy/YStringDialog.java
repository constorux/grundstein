package YComponentsLegacy;

import YComponents.YButton;
import YComponents.YTextField;
import YComponents.YTheme;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



public class YStringDialog {

	private String _text;
	
	public YStringDialog(String Title, String ButtonText, String TextFieldText)
	{
		_text = "";
		
		JDialog dialog = new JDialog();
		dialog.setTitle(Title);
		dialog.setBackground(YTheme.getColor(YTheme.C_ACCENT));
		dialog.setBounds(100, 100, 389, 92);
		dialog.getContentPane().setLayout(new BorderLayout());
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		dialog.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(5, 0));
			
		YButton ybtn_go = new YButton(ButtonText);
		ybtn_go.setBackground(YTheme.getColor(YTheme.C_PRIMARY));
		ybtn_go.setPreferredSize(new Dimension(50, 10));
		panel.add(ybtn_go, BorderLayout.EAST);
		
		YTextField ytf_enter = new YTextField();
		ytf_enter.setText(TextFieldText);
		panel.add(ytf_enter, BorderLayout.CENTER);
		//ytf_enter.setColumns(10);
		
		dialog.revalidate();
		dialog.repaint();
		dialog.setVisible(true);
		
		ybtn_go.addActionListener( new ActionListener()
		{ 
			public void actionPerformed(ActionEvent e) 
			{
				_text = ybtn_go.getText();
				
			}
		});
	}
	
	public  String get()
	{
		return _text;
	}
	
}
