package YComponentsLegacy;

import YComponents.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;


public class YChat extends JPanel
{
	Color 	_color;
	Container _comp_MainArea_Items;
	YScrollPane _yscp_MainArea;
	YLabel _ylbl_ChatName;
	YButton ybtn_Controls_Send;
	YTextField ytf_Controls_Text;
	
	public YChat() 
	{	  
		
		setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_DARK_4));
		setVisible(false);
		setBounds(101, 89, 268, 367);
		setLayout(new BorderLayout(0, 0));
		
		YPanel ypan_Header = new YPanel();
		ypan_Header.setPreferredSize(new Dimension(10, 40));
		ypan_Header.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_DARK_3));
		ypan_Header.setBorder(new EmptyBorder(5, 5, 5, 5));
		add(ypan_Header, BorderLayout.NORTH);
		ypan_Header.setLayout(new BorderLayout(0, 0));
		
		_ylbl_ChatName = new YLabel();
		_ylbl_ChatName.setBounds(80, 130, 200, 20);
		ypan_Header.add(_ylbl_ChatName);
		_ylbl_ChatName.setFont(YTheme.F_BOLD(12));
		_ylbl_ChatName.setText("loading...");
		
		YPanel ypan_Controls = new YPanel();
		ypan_Controls.setPreferredSize(new Dimension(10, 36));
		ypan_Controls.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_DARK_3));
		ypan_Controls.setBorder(new EmptyBorder(3, 3, 3, 3));
		add(ypan_Controls, BorderLayout.SOUTH);
		ypan_Controls.setLayout(new BorderLayout(0, 0));
		
		ybtn_Controls_Send = new YButton(30, false, true, true, false);
		ybtn_Controls_Send.setPreferredSize(new Dimension(35, 30));
		ybtn_Controls_Send.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_DARK_2));
		ypan_Controls.add(ybtn_Controls_Send, BorderLayout.EAST);
		ybtn_Controls_Send.setIcon(new ImageIcon(YChat.class.getResource("/YComponents/resources/yb_send.png")), 0.8);
		
		ytf_Controls_Text = new YTextField(30, true, false, false, true, "");
		ypan_Controls.add(ytf_Controls_Text, BorderLayout.CENTER);
		ytf_Controls_Text.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_DARK_2));
		ytf_Controls_Text.setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_1));
		ytf_Controls_Text.setText("");
		
		_yscp_MainArea = new YScrollPane();
		add(_yscp_MainArea, BorderLayout.CENTER);
		
		
		_comp_MainArea_Items = new Container();
		_comp_MainArea_Items.setLayout(new BoxLayout(_comp_MainArea_Items, BoxLayout.Y_AXIS));
		_yscp_MainArea.getViewport().setView(_comp_MainArea_Items);
		
		
		
		
		ybtn_Controls_Send.addActionListener( new ActionListener()
		{ 
			public void actionPerformed(ActionEvent e) 
			{
				_yscp_MainArea.revalidate();
				_yscp_MainArea.ScrollToMaxmum();
				
			}
		});
	   
    }
  
  
    
  public YButton SendButton()
  {
	  return ybtn_Controls_Send;
  }
  
  public YTextField SendTextField()
  {
	  return ytf_Controls_Text;
  }

  public void setBackground(Color color)
  {
  		_color = color;
  		super.setBackground(_color);
  }
  
  public void clear()
  {
	  _comp_MainArea_Items.removeAll();
  }
  
  public void loadNewChat(String Title)
  {
	  clear();
	  _ylbl_ChatName.setText(Title);
	  
  }

  public void addMessage(boolean own,String Username, String Time, String Message)
  {
	  _comp_MainArea_Items.add(new YMessage(getWidth() - 20, own, Username, Time, Message));
	  revalidate();
	  _yscp_MainArea.ScrollToMaxmum();
	  
  }
  
  public void addFileMessage(String Username, String Time, String FileID, String Filename, ImageIcon Icon)
  {
	  YMessage message = new YMessage(getWidth() - 20, false, Username, Time);
	  message.setFile(FileID, Filename, Icon);
	  _comp_MainArea_Items.add(message);
	  revalidate();
	  _yscp_MainArea.ScrollToMaxmum();
	  
  }

 
  




  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}