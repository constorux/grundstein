package YComponentsLegacy;

import YComponents.YTheme;

import java.awt.*;
import java.awt.geom.*;

import javax.swing.*;

public class YMenuButton extends JButton 
{
	
	Color 	_color;
	Shape 	_shape;
	
	
	
	
  public YMenuButton() 
  {
	Dimension size = getPreferredSize();
    size.width = size.height = Math.max(size.width, size.height);
    
    setForeground(YTheme.getColor(YTheme.C_OBJECT_FOREGROUND));
    setFont(YTheme.F_REGULAR(12));
    setFocusPainted(false);
    setBorderPainted(false);
    setContentAreaFilled(false);
    setPreferredSize(size);
  
  }
  
  
  
  
  
  
  
  protected void paintComponent(Graphics g) 
  {
	  Graphics2D g2d = (Graphics2D) g.create();
	  g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	   
	   
	  if (getModel().isArmed()) 
	  {
		  g2d.setColor(YTheme.getColor(YTheme.C_PRIMARY));
	  } 
	  else 
	  {
		  g2d.setColor(YTheme.getColor(YTheme.C_WHITE));
	  }
   

	  g2d.fill(_shape = CustomArea());
	  g2d.dispose();
    
	  super.paintComponent(g);
    
	  g2d.dispose();
  }


 
  
  
  Area CustomArea() 
  {
	  
	  Area area = new Area(new RoundRectangle2D.Float(getWidth()/4, (getHeight()/20)*5,  getWidth()/2, getHeight()/10, getHeight()/15, getHeight()/15));
	  area.add(new Area(   new RoundRectangle2D.Float(getWidth()/4, (getHeight()/20)*9,  getWidth()/2, getHeight()/10, getHeight()/15, getHeight()/15)));
	  area.add(new Area(   new RoundRectangle2D.Float(getWidth()/4, (getHeight()/20)*13, getWidth()/2, getHeight()/10, getHeight()/15, getHeight()/15)));
	  
	  
	return area; 
  }
  
  
}