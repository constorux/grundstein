package YComponentsLegacy;

import YComponents.YLabel;
import YComponents.YTheme;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class YSettingsItem extends JPanel 
{
	JComponent _component;
	
  public YSettingsItem(String ItemName, JComponent component) 
  {
	  _component = component;
	  
	  int preferredHeight = 50;
	
	  if((_component != null)&&(_component.getPreferredSize().getHeight() > 50))
	  {
		  preferredHeight = (int) _component.getPreferredSize().getHeight();
	  }
	  
    setPreferredSize(new Dimension(550, preferredHeight));
    setMinimumSize(new Dimension(550, preferredHeight));
    setMaximumSize(new Dimension(550, preferredHeight));
	setBorder(new EmptyBorder(10, 10, 10, 10));
	setLayout(new BorderLayout(0, 0));
	setOpaque(false);
	
	YLabel lbl_Text = new YLabel();
	lbl_Text.setText(ItemName);
	lbl_Text.setVerticalAlignment(SwingConstants.TOP);
	lbl_Text.setForeground(YTheme.getColor(YTheme.C_TEXT_DARK_4));
	lbl_Text.setBorder(new EmptyBorder(10, 10, 10, 10));
	add(lbl_Text, BorderLayout.CENTER);
	
	if(_component != null)
	{
		add(_component, BorderLayout.EAST);
	}
  
  } 
  
  public JComponent getComponent()
  {
	  return _component;
  }
 
  
}