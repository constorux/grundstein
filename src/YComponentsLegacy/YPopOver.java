package YComponentsLegacy;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.RoundRectangle2D;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class YPopOver extends JFrame{
	
	public YPopOver()
	{
		getContentPane().setBackground(Color.BLACK);
		setLayout(new BorderLayout());
		setUndecorated(true);
		setSize(200, 200);
		setOpacity(0);

		JPanel pnlButtons = new JPanel();
		pnlButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		pnlButtons.setOpaque(false);
		pnlButtons.add(new JButton("<<"));
		pnlButtons.add(new JButton("<"));
		pnlButtons.add(new JButton(">"));
		pnlButtons.add(new JButton(">>"));

		// Okay, in theory, getContentPane() is required, but better safe the sorry
		getContentPane().add(pnlButtons, BorderLayout.SOUTH);

		setVisible(true);
	}
	
	
}
