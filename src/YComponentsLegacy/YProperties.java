package YComponentsLegacy;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import YComponents.YButton;
import YComponents.YLabel;
import YComponents.YTheme;

public class YProperties extends JPanel{

	JPanel _pan_content;
	YLabel _ylbl_title;
	
	public YProperties()
	{
		setPreferredSize(new Dimension(200, 10));
		setLayout(new BorderLayout(0, 0));
		setBackground(YTheme.getColor(YTheme.C_WHITE));
		
		JPanel pan_Header = new JPanel();
		pan_Header.setOpaque(false);
		pan_Header.setPreferredSize(new Dimension(10, 45));
		add(pan_Header, BorderLayout.NORTH);
		pan_Header.setBorder(new EmptyBorder(5, 5, 5, 5));
		pan_Header.setLayout(new BorderLayout(0, 0));
		
		YButton ybtn_back = new YButton();
		//ybtn_back.setIcon(new ImageIcon( YProperties.class.getResource("/images/yc_button_back.png")), 0.4);
		ybtn_back.setPreferredSize(new Dimension(35, 35));
		ybtn_back.setEdgeRoundness(40);
		ybtn_back.setBackground(YTheme.getColor(YTheme.C_TEXT_LIGHT_3));
		pan_Header.add(ybtn_back, BorderLayout.WEST);
		
		_ylbl_title = new YLabel("title");
		_ylbl_title.setForeground(YTheme.getColor(YTheme.C_PRIMARY));
		_ylbl_title.setHorizontalTextPosition(SwingConstants.CENTER);
		_ylbl_title.setHorizontalAlignment(SwingConstants.CENTER);
		pan_Header.add(_ylbl_title, BorderLayout.CENTER);
		
		_pan_content = new JPanel();
		_pan_content.setOpaque(false);
		_pan_content.setBorder(null);
		add(_pan_content, BorderLayout.CENTER);
		_pan_content.setLayout(new BoxLayout(_pan_content, BoxLayout.Y_AXIS));
	
		
		/*ybtn_back.addActionListener( new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				setVisible(false);
			}
		});*/
	}
	
	@Override public Component add(Component c)
	  {
	  		return _pan_content.add(c);
	  }
	
	@Override public void removeAll()
	  {
	  		_pan_content.removeAll();
	  }
	
	public void setTitle(String Title)
	{
		_ylbl_title.setText(Title);
	}
}
