package YComponentsLegacy;

import YComponents.YTheme;

import java.awt.*;
import javax.swing.*;

public class YRadioButton extends JRadioButton 
{
  

	public YRadioButton() 
	{
    
		this("");
  
	}
	
	 public YRadioButton(String text) 
	 {
		 setText(text);
		 Dimension size = getPreferredSize();
		 size.width = size.height = Math.max(size.width, size.height);
		    
		 setForeground(YTheme.getColor(YTheme.C_OBJECT_FOREGROUND));
		 setFont(YTheme.F_REGULAR(12));
		 setPreferredSize(size);
		 setFocusPainted(false);
	 }
}