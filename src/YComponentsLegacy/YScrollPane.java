package YComponentsLegacy;

import javax.swing.*;

public class YScrollPane extends JScrollPane
{
	JScrollBar _sbv;
	
	public YScrollPane() 
	{	  
		setOpaque(false);
		setBorder(null);
		getVerticalScrollBar().setUnitIncrement(16);
		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		getViewport().setOpaque(false);
		
		
		_sbv = getVerticalScrollBar();
        _sbv.setUI(new YScrollbarUI());
        _sbv.setOpaque(false);
    }
	
	public void ScrollToMaxmum()
	{
		getVerticalScrollBar();
		_sbv.setValue( _sbv.getMaximum() );
	}
}