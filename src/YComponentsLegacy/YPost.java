package YComponentsLegacy;

import YComponents.YButton;
import YComponents.YLabel;
import YComponents.YPanel;
import YComponents.YTheme;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class YPost extends JPanel
{
	
  public YPost(String Title, String UserName, String Text, int PostID) 
  {
	  	setBorder(new EmptyBorder(20, 20, 20, 20));
	  	setBounds(420, 52, 300, 201);
		setLayout(new BorderLayout(0, 0));
		setPreferredSize(new Dimension(400, 250));
		setMaximumSize(new Dimension(400, 250));
		setOpaque(false);
		
	  	YPanel pan_Base = new YPanel();
	  	pan_Base.setOpaque(false);
	  	pan_Base.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND));
	  	pan_Base.setLayout(new BorderLayout(0, 0));
	  	add(pan_Base, BorderLayout.CENTER);
		
		
		JPanel pan_Header = new JPanel();
		pan_Header.setPreferredSize(new Dimension(10, 40));
		pan_Base.add(pan_Header, BorderLayout.NORTH);
		pan_Header.setLayout(new BorderLayout(0, 0));
		
		
		
		YButton btn_Header_title = new YButton(10, false, true, false, false);
		pan_Header.add(btn_Header_title, BorderLayout.CENTER);
		btn_Header_title.setPreferredSize(new Dimension(30, 10));
		btn_Header_title.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_LIGHT_4));
		btn_Header_title.setText(Title);
		
		YButton btn_Header_Creator = new YButton(10, true, false, false, false);
		pan_Header.add(btn_Header_Creator, BorderLayout.WEST);
		btn_Header_Creator.setText(UserName);
		btn_Header_Creator.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_LIGHT_2));
		btn_Header_Creator.setPreferredSize(new Dimension(80, 10));
		
		
		/*
		YButton btn_expandPost = new YButton(10, false, false, true, true);
		btn_expandPost.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		pan_Base.add(btn_expandPost, BorderLayout.SOUTH);
		btn_expandPost.setText("expand post");
		btn_expandPost.setPreferredSize(new Dimension(10, 30));
		btn_expandPost.setBackground(new Color(192, 192, 192));
		*/
		
		
		JPanel pan_Body = new JPanel();
		pan_Body.setBackground(YTheme.getColor(YTheme.C_OBJECT_BACKGROUND_LIGHT_5));
		pan_Base.add(pan_Body, BorderLayout.CENTER);
		pan_Body.setLayout(new BorderLayout(0, 0));
		
		JPanel pan_Body_controls = new JPanel();
		pan_Body_controls.setBorder(new EmptyBorder(5, 5, 5, 5));
		pan_Body_controls.setPreferredSize(new Dimension(10, 30));
		pan_Body_controls.setOpaque(false);
		pan_Body.add(pan_Body_controls, BorderLayout.SOUTH);
		pan_Body_controls.setLayout(new BorderLayout(0, 0));
		
		YButton btn_Comment = new YButton(10, true, true, true, true);
		btn_Comment.setText("pin");
		btn_Comment.setPreferredSize(new Dimension(40, 10));
		btn_Comment.setBackground(YTheme.getColor(YTheme.C_ACCENT));
		pan_Body_controls.add(btn_Comment, BorderLayout.EAST);
		
		YVoteTool yvt_Vote = new YVoteTool(16);
		pan_Body_controls.add(yvt_Vote, BorderLayout.WEST);
		yvt_Vote.refresh();
		
		JPanel pan_Body_content = new JPanel();
		pan_Body_content.setBorder(new EmptyBorder(10, 10, 10, 10));
		pan_Body_content.setOpaque(false);
		pan_Body.add(pan_Body_content, BorderLayout.CENTER);
		pan_Body_content.setLayout(new BorderLayout(0, 0));
		
		YLabel lbl_Body = new YLabel();
		lbl_Body.setText(Text);
		lbl_Body.setForeground(YTheme.getColor(YTheme.C_TEXT_LIGHT_1));
		lbl_Body.setBorder(new EmptyBorder(10, 10, 10, 10));
		pan_Body_content.add(lbl_Body, BorderLayout.CENTER);
	  
				
  }
  
}