package YComponentsLegacy;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import YComponents.YLabel;

public class YPropertyItem extends JPanel{

	public YPropertyItem( String Label, JComponent comp)
	{
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setOpaque(false);
		setLayout(new BorderLayout(0, 0));
		setPreferredSize(new Dimension(200, 40));
		setMaximumSize(new Dimension(200, 40));
		
		YLabel label = new YLabel(Label);
		label.setForeground(new Color(30, 30, 30));
		add(label, BorderLayout.CENTER);
		
		comp.setPreferredSize(new Dimension(110, 10));
		add(comp, BorderLayout.EAST);
	
	}
}
