package GComponents;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class RoundedShape extends ShapeComponent {
    private Boolean _LeftUpperRound;
    private Boolean _RightUpperRound;
    private Boolean _RightLowerRound;
    private Boolean _LeftLowerRound;
    private int _edgeRound;
    private boolean _opaque = true;
    Color _color;

    public RoundedShape() {
        this(10, true, true, true, true);
    }

    public RoundedShape(int edgeRound) {
        this(edgeRound, true, true, true, true);
    }

    public RoundedShape(int EdgeRound, Boolean LeftUpperRound, Boolean RightUpperRound, Boolean RightLowerRound, Boolean LeftLowerRound) {
        _edgeRound = EdgeRound;

        _LeftUpperRound = LeftUpperRound;
        _RightUpperRound = RightUpperRound;
        _RightLowerRound = RightLowerRound;
        _LeftLowerRound = LeftLowerRound;

        //setBorder(new EmptyBorder(1, 5, 1, 5));
    }



    @Override ArrayList<Region> buildRegions() {

        Area area = new Area(new RoundRectangle2D.Float(0, 0, getWidth(), getHeight(), _edgeRound, _edgeRound));


        if (!_LeftUpperRound) {
            area.add(new Area(new Rectangle(0, 0, getWidth() / 2, getHeight() / 2)));
        }
        if (!_RightUpperRound) {
            area.add(new Area(new Rectangle(getWidth() / 2 + 1, 0, getWidth() / 2, getHeight() / 2)));
        }
        if (!_RightLowerRound) {
            area.add(new Area(new Rectangle(getWidth() / 2 + 1, getHeight() / 2 + 1, getWidth() / 2, getHeight() / 2)));
        }
        if (!_LeftLowerRound) {
            area.add(new Area(new Rectangle(0, getHeight() / 2 + 1, getWidth() / 2, getHeight() / 2)));
        }

        ArrayList<Region> regions = new ArrayList<Region>();
        regions.add(new Region(area, _color));
        return (regions);
    }




    //end public methods


    //start public methods | setters

    public void setRoundEdges(Boolean LeftUpperRound, Boolean RightUpperRound, Boolean RightLowerRound, Boolean LeftLowerRound) {
        _LeftUpperRound = LeftUpperRound;
        _RightUpperRound = RightUpperRound;
        _RightLowerRound = RightLowerRound;
        _LeftLowerRound = LeftLowerRound;
    }

    public void setEdgeRoundness(int edgeRound) {
        _edgeRound = edgeRound;
    }

    public void setColor(Color color)
    {
        _color = color;
        repaint();
    }

    public Color getColor(){
        return _color;
    }

}
