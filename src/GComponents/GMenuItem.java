package GComponents;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

import GComponents.*;


public class GMenuItem extends JButton
{
	String _ID;
	JPanel _pan_ItemBase;

//	public YMenuItem(String title, ImageIcon ItemIcon)
//	{
//		 this(ID, ID, ItemIcon);
//	}
//
	public GMenuItem(GContentPane p, ImageIcon itemIcon)
	{
		 this(p.getPaneID(), p.getPaneTitle(), itemIcon);
	}

	public GMenuItem(String ID , String Title, ImageIcon itemIcon)
	{
		_ID = ID;

		Dimension size = getPreferredSize();
		size.width = 180; 
		size.height = 40;
		
		if(itemIcon == null)
		{
			itemIcon = new ImageIcon(GMenuItem.class.getResource("/GComponents/resources/yb_send.png"));
		}
		
		setForeground(GTheme.getColor(GTheme.C_TEXT_LIGHT_1));
		setLayout(new BorderLayout(0, 0));
		setPreferredSize(size);
		setOpaque(false);
		setMargin(new Insets(0,0,0,0));
		setBorder(null);
		setContentAreaFilled(false);
		
		
		_pan_ItemBase = new JPanel();
		_pan_ItemBase.setBackground(GTheme.getColor(GTheme.C_MENU_ITEM));
		add(_pan_ItemBase,  BorderLayout.CENTER);
		_pan_ItemBase.setBorder(new EmptyBorder(10, 10, 10, 10));
		_pan_ItemBase.setLayout(new BorderLayout(0, 0));
		  
	
		  GLabel lbl_MenuItem = new GLabel();
		  lbl_MenuItem.setColor(GTheme.getColor(GTheme.C_TEXT_LIGHT_4));
		lbl_MenuItem.setText(Title);
		lbl_MenuItem.setForeground(new Color(250, 250, 250));
		_pan_ItemBase.add(lbl_MenuItem, BorderLayout.CENTER);
	
	
		GLabel lbl_ItemIcon = new GLabel();
		lbl_ItemIcon.setPreferredSize(new Dimension(20,20));
		//lbl_ItemIcon.setVerticalAlignment(SwingConstants.CENTER);
		//lbl_ItemIcon.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_ItemIcon.setIcon(itemIcon);
		_pan_ItemBase.add(lbl_ItemIcon, BorderLayout.EAST);
	} 
	
	public String getID()
	{
		return _ID;
	}
	
	public void setColor(Color color)
	{
		_pan_ItemBase.setBackground(color);
	}
}