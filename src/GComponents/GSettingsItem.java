package GComponents;


import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class GSettingsItem extends GPanel {
    JComponent _component;

    public GSettingsItem(String ItemName, JComponent component) {
        super(10);

        _component = component;
        int preferredHeight = 70;

        if ((_component != null) && (_component.getPreferredSize().getHeight() > 50)) {

            preferredHeight = (int) _component.getPreferredSize().getHeight();


        }

        setWidth(550);
        setBorder(new EmptyBorder(7, 7, 7, 7));
        setLayout(new BorderLayout(0, 0));

        setColor(GTheme.getColor(GTheme.C_OBJECT_BACKGROUND_LIGHT_7));

        GLabel lbl_Text = new GLabel();
        lbl_Text.setText(ItemName);
        //lbl_Text._baseObject.size
        //lbl_Text.setVerticalAlignment(SwingConstants.TOP);
        //lbl_Text.setBorder(new EmptyBorder(10, 10, 10, 10));
        lbl_Text.setColor(GTheme.getColor(GTheme.C_TEXT_DARK_3));
        add(lbl_Text, BorderLayout.CENTER);

        if (_component != null) {
            add(_component, BorderLayout.EAST);
        }

    }

    public JComponent getComponent() {
        return _component;
    }


}