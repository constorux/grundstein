package GComponents;

import java.awt.Color;
import java.awt.Font;

public class GTheme
{

    private static boolean _initiated = false;
    private static Color _fallBackColor = new Color(175,0,199);
    private static Color[] _colors = new Color[100000];

    public final static Font F_REGULAR(int Size)
    {
        return new Font("Lato Medium", Font.PLAIN, Size);
    }

    public final static Font F_BOLD(int Size)
    {
        return new Font("Lato Heavy", Font.PLAIN, Size);
    }


    private static void initiate()
    {
        //Fall Back color
        _colors[0] = _fallBackColor;

        _colors[10000] = new Color(255, 255, 255);
        _colors[10001] = new Color(0, 0, 0);
        _colors[10002] = new Color(50, 50, 200);
        _colors[10003] = new Color(255, 215, 0);
        _colors[10004] = new Color(139, 69, 19);
        _colors[10005] = new Color(255, 140, 0);
        _colors[10006] = new Color(110, 45, 156);
        _colors[10007] = new Color(46, 139, 87);
        _colors[10008] = new Color(128, 0, 0);
        _colors[10009] = new Color(0, 0, 0, 0);

        _colors[20000] = new Color(110, 45, 156);
        _colors[20001] = new Color(65, 105, 225);


        _colors[30000] = new Color(225, 225, 225);

        _colors[31000] = new Color(150, 150, 150);
        _colors[31001] = new Color(170, 170, 170);
        _colors[31002] = new Color(180, 180, 180);
        _colors[31003] = new Color(190, 190, 190);
        _colors[31004] = new Color(210, 210, 210);
        _colors[31005] = new Color(230, 230, 230);
        _colors[31006] = new Color(235, 235, 235);
        _colors[31007] = new Color(245, 245, 245);

        _colors[32000] = new Color(90, 90, 90);
        _colors[32001] = new Color(70, 70, 70);
        _colors[32002] = new Color(50, 50, 50);
        _colors[32003] = new Color(40, 40, 40);
        _colors[32004] = new Color(20, 20, 20);


        _colors[40000] = new Color(255, 255, 255);
        _colors[40001] = new Color(110, 110, 110);
        _colors[40002] = new Color(50, 50, 200);

        _colors[41000] = new Color(250, 250, 250);
        _colors[41001] = new Color(230, 230, 230);
        _colors[41002] = new Color(210, 210, 210);
        _colors[41003] = new Color(190, 190, 190);
        _colors[41004] = new Color(170, 170, 170);

        _colors[42000] = new Color(100, 100, 100);
        _colors[42001] = new Color(70, 70, 70);
        _colors[42002] = new Color(50, 0, 50);
        _colors[42003] = new Color(30, 30, 30);
        _colors[42004] = new Color(20, 20, 20);


        _colors[50000] = new Color(40, 40, 40);
        _colors[50001] = new Color(25, 25, 25);
        _colors[50002] = new Color(40, 40, 40);


        _initiated = true;
    }


    //colors

    //colors -> basic
    public final static int C_WHITE = 10000;
    public final static int C_BLACK = 10001;
    public final static int C_BLUE = 10002;
    public final static int C_YELLOW = 10003;
    public final static int C_BROWN = 10004;
    public final static int C_ORANGE = 10005;
    public final static int C_PURPLE = 10006;
    public final static int C_GREEN = 10007;
    public final static int C_RED = 10008;
    public final static int C_TRANSPARENT = 10009;


    //colors -> theme

    public final static int C_PRIMARY = 20000;
    public final static int C_ACCENT = 20001;

    public final static int C_BACKGROUND = 20002;
    public final static int C_CONTROL = 20003;


    //colors -> background

    public final static int C_OBJECT_BACKGROUND = 30000;

    public final static int C_OBJECT_BACKGROUND_LIGHT_1 = 31000;
    public final static int C_OBJECT_BACKGROUND_LIGHT_2 = 31001;
    public final static int C_OBJECT_BACKGROUND_LIGHT_3 = 31002;
    public final static int C_OBJECT_BACKGROUND_LIGHT_4 = 31003;
    public final static int C_OBJECT_BACKGROUND_LIGHT_5 = 31004;
    public final static int C_OBJECT_BACKGROUND_LIGHT_6 = 31005;
    public final static int C_OBJECT_BACKGROUND_LIGHT_7 = 31006;
    public final static int C_OBJECT_BACKGROUND_LIGHT_8 = 31007;

    public final static int C_OBJECT_BACKGROUND_DARK_1 = 32000;
    public final static int C_OBJECT_BACKGROUND_DARK_2 = 32001;
    public final static int C_OBJECT_BACKGROUND_DARK_3 = 32002;
    public final static int C_OBJECT_BACKGROUND_DARK_4 = 32003;
    public final static int C_OBJECT_BACKGROUND_DARK_5 = 32004;


    //colors -> foreground

    public final static int C_OBJECT_FOREGROUND = 40000;
    public final static int C_OBJECT_BACKGROUND_INACTIVE = 40001;
    public final static int C_OBJECT_BACKGROUND_PRESSED = 40002;

    public final static int C_TEXT_LIGHT_1 = 41000;
    public final static int C_TEXT_LIGHT_2 = 41001;
    public final static int C_TEXT_LIGHT_3 = 41002;
    public final static int C_TEXT_LIGHT_4 = 41003;
    public final static int C_TEXT_LIGHT_5 = 41004;

    public final static int C_TEXT_DARK_1 = 42000;
    public final static int C_TEXT_DARK_2 = 42001;
    public final static int C_TEXT_DARK_3 = 42002;
    public final static int C_TEXT_DARK_4 = 42003;
    public final static int C_TEXT_DARK_5 = 42004;


    //colors -> menu

    public final static int C_MENU_ITEM = 50000;
    public final static int C_MENU_HEADER = 50001;
    public final static int C_MENU_BODY = 50002;





    public static Color getColor(int colorCode)
    {
        if(!_initiated)
        {
            initiate();
        }
        if(colorCode < _colors.length)
        {
            Color value = _colors[colorCode];
            if(value != null)
            {
                return value;
            }
        }
        return _fallBackColor;
    }


    //legacy


	/*
	//colors
	public final static Color C_PRIMARY = new Color(110, 45, 156);
	public final static Color C_ACCENT = new Color(65, 105, 225);

	public final static Color C_BACKGROUND = new Color(240, 240, 240);
	public final static Color C_CONTROL = new Color(250,250,250);

	public final static Color C_MENU_ITEM = new Color(40, 40, 40);
	//public final static Color C_MENU_ITEM = new Color(30, 30, 30);
	public final static Color C_MENU_HEADER = new Color(25, 25, 25);
	public final static Color C_MENU_BODY = new Color(40, 40, 40);

	public final static Color C_OBJECT_BACKGROUND    		= new Color(230,230,230);

	public final static Color C_OBJECT_BACKGROUND_LIGHT	    = new Color(245,245,245);
	public final static Color C_OBJECT_BACKGROUND_LIGHT_7	= new Color(240,240,240);
	public final static Color C_OBJECT_BACKGROUND_LIGHT_6	= new Color(230,230,230);
	public final static Color C_OBJECT_BACKGROUND_LIGHT_5	= new Color(210,210,210);
	public final static Color C_OBJECT_BACKGROUND_LIGHT_4 	= new Color(190,190,190);
	public final static Color C_OBJECT_BACKGROUND_LIGHT_3 	= new Color(180,180,180);
	public final static Color C_OBJECT_BACKGROUND_LIGHT_2 	= new Color(170,170,170);
	public final static Color C_OBJECT_BACKGROUND_LIGHT_1 	= new Color(150,150,150);

	public final static Color C_OBJECT_BACKGROUND_DARK_1	= new Color(90,90,90);
	public final static Color C_OBJECT_BACKGROUND_DARK_2 	= new Color(70,70,70);
	public final static Color C_OBJECT_BACKGROUND_DARK_3 	= new Color(50,50,50);
	public final static Color C_OBJECT_BACKGROUND_DARK_4 	= new Color(40,40,40);
	public final static Color C_OBJECT_BACKGROUND_DARK_5 	= new Color(20,20,20);



	public final static Color C_OBJECT_FOREGROUND = new Color(255, 255, 255);
	public final static Color C_OBJECT_BACKGROUND_INACTIVE = new Color(110, 110, 110);
	public final static Color C_OBJECT_BACKGROUND_PRESSED = new Color(50, 50, 200);

	public final static Color C_TEXT_LIGHT_1 = new Color(250, 250, 250);
	public final static Color C_TEXT_LIGHT_2 = new Color(230, 230, 230);
	public final static Color C_TEXT_LIGHT_3 = new Color(210, 210, 210);
	public final static Color C_TEXT_LIGHT_4 = new Color(190, 190, 190);
	public final static Color C_TEXT_LIGHT_5 = new Color(170, 170, 170);

	public final static Color C_TEXT_DARK_1 = new Color(100, 100, 100);
	public final static Color C_TEXT_DARK_2 = new Color(70, 70, 70);
	public final static Color C_TEXT_DARK_3 = new Color(50, 0, 50);
	public final static Color C_TEXT_DARK_4 = new Color(30, 30, 30);
	public final static Color C_TEXT_DARK_5 = new Color(20, 20, 20);

	public final static Color C_WHITE = new Color(255, 255, 255);
	public final static Color C_BLACK =
	public final static Color C_BLUE = new Color(50, 50, 200);
	public final static Color C_YELLOW = new Color(255, 215, 0);
	public final static Color C_BROWN = new Color(139,69,19);
	public final static Color C_ORANGE = new Color(255, 140, 0);
	public final static Color C_PURPLE = new Color(110, 45, 156);
	public final static Color C_GREEN = new Color(46, 139, 87);
	public final static Color C_RED = new Color(128, 0, 0);

	*/

}
