package GComponents.tools;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

public class GUtilities {

	public static ImageIcon ShrinkImageToComponent(ImageIcon icon, JComponent c , double scale)
	  {
		 double scaleToX = (c.getSize().getWidth()  * scale);
		 double scaleToY = (c.getSize().getHeight() * scale);
		 
		 
		
		 if(scaleToX == 0)
		 {
			 scaleToX = (c.getPreferredSize().getWidth() * scale);
			 
			 if(scaleToX == 0)
			 {
				 scaleToX = 100;
			 }
		 }
		 
		 
		 
		 if(scaleToY == 0)
		 {
			 scaleToY = (c.getPreferredSize().getHeight() * scale);
			 
			 if(scaleToY == 0)
			 {
				 scaleToY = 100;
			 }
		 }
		 
		 

		 int scaleX = icon.getIconWidth();
		 int scaleY = icon.getIconHeight();
		 
		
		 if ((((int) scaleToX) == 0 ) || (((int) scaleToY == 0)))
		 {
			 scaleToX = (c.getWidth()  * scale);
			 scaleToY = (c.getHeight() * scale);
		 }
		
		 double CompareX = scaleToX / scaleX;
		 double CompareY = scaleToY / scaleY;
		
		 scaleX = (int) (CompareX * scaleX);
		 scaleY = (int) (CompareX * scaleY);
		 
		 if(scaleY > scaleToY)
		 {
			 scaleX = (int) (CompareY * icon.getIconWidth());
			 scaleY = (int) (CompareY * icon.getIconHeight());
		 }
		 
		
		 return new ImageIcon(icon.getImage().getScaledInstance(scaleX, scaleY, java.awt.Image.SCALE_SMOOTH));
	  }
}
