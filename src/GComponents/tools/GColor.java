package GComponents.tools;

import java.awt.Color;

public class GColor extends Color
{
	public GColor(Color c)
	{
		this(c.getRed(), c.getGreen(), c.getBlue());
	}

	public GColor(int r, int g, int b)
	{
		
		super(r, g, b);
		
	}

}
