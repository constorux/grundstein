package GComponents.tools;

import java.awt.*;
import java.awt.geom.Area;
import java.util.LinkedList;

public class GShape
{
    LinkedList<Area> _areas = new LinkedList<Area>();
    LinkedList<Color> _colors = new LinkedList<Color>();

    public GShape()
    {

    }

    public void addArea(Area a, Color c)
    {
        _areas.add(a);
        _colors.add(c);
    }


}
