package GComponents;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class GContentPane extends GPanel
{
    private GPanel _pan_Footer;
    private GPanel _pan_Content;
    private Boolean _showFooter;
    private GPanel _pan_FooterBase;
    private String _ID;
    private String _title;

    private GPanel _pan_Header;
    private GLabel _ylbl_PageTitle;


    public GContentPane()
    {
        this(false, "unnamed pane", "");
    }

    public GContentPane(Boolean showFooter)
    {
        this(showFooter, "unnamed pane", "");
    }

    public GContentPane(String ID)
    {
        this(false, ID, "unnamed pane");
    }

    public GContentPane(String ID, String title)
    {
        this(false, ID, title);
    }

    public GContentPane(Boolean showFooter, String ID, String title)
    {


        _showFooter = showFooter;
        _title = title;
        _ID = ID;
        super.setLayout(new BorderLayout(0, 0));

        GPanel ContentBase = new GPanel();
        //ContentBase.setOpaque(false);
        ContentBase.setLayout(new BorderLayout(0, 0));
        ContentBase.setColor(GTheme.getColor(GTheme.C_OBJECT_BACKGROUND_LIGHT_8));
        add(ContentBase, BorderLayout.CENTER);

        _pan_Content = new GPanel();
        _pan_Content.setOpaque(false);
        _pan_Content.setColor(new Color(0,255,0));
        _pan_Content.setLayout(new BoxLayout(_pan_Content, BoxLayout.Y_AXIS));
        ContentBase.add(_pan_Content, BorderLayout.CENTER);

        _pan_FooterBase = new GPanel();
        _pan_FooterBase.setPreferredSize(new Dimension(40, 40));
        _pan_FooterBase.setMaximumSize(new Dimension(30000, 40));
        _pan_FooterBase.setColor(GTheme.getColor(GTheme.C_OBJECT_BACKGROUND_LIGHT_5));
        add(_pan_FooterBase, BorderLayout.SOUTH);
        _pan_FooterBase.setLayout(new BorderLayout(0, 0));
        _pan_FooterBase.setVisible(_showFooter);

        _pan_Footer = new GPanel();
        _pan_Footer.setPreferredSize(new Dimension(10, 40));
        //_pan_Footer.setOpaque(false);
        _pan_Footer.setOpaque(false);
        _pan_FooterBase.add(_pan_Footer, BorderLayout.CENTER);
        _pan_Footer.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        _pan_Header = new GPanel();
        _pan_Header.setPreferredSize(new Dimension(40, 40));
        _pan_Header.setColor(GTheme.getColor(GTheme.C_OBJECT_BACKGROUND_LIGHT_5));
        add(_pan_Header, BorderLayout.NORTH);
        _pan_Header.setLayout(new BorderLayout(0, 0));
        _pan_Header.setVisible(false);

        _ylbl_PageTitle = new GLabel("");
        _ylbl_PageTitle.setFont(GTheme.F_REGULAR(18));
        _ylbl_PageTitle.setForeground(GTheme.getColor(GTheme.C_TEXT_DARK_3));
        _ylbl_PageTitle.setPreferredSize(new Dimension(400, 40));
        _ylbl_PageTitle.setBorder(new EmptyBorder(7, 7, 7, 7));
        //ylbl_PageTitle.setBackground(GTheme.C_ACCENT);
        _pan_Header.add(_ylbl_PageTitle, BorderLayout.WEST);


        //setting it up
        if ((title != null) && (!title.equals("")))
        {
            setHeaderTitle(title);
        }
    }


    @Override
    public Component add(Component c)
    {

        return _pan_Content.add(c);
    }

//  @Override public void setLayout(LayoutManager mgr)
//  {
//	  setContentLayout(mgr);
//  }

    @Override public void setLayout(LayoutManager mgr)
    {
        _pan_Content.setLayout(mgr);
    }

    public void resetContentLayout()
    {
        _pan_Content.setLayout(null);
    }


    public GButton addFooterButton()
    {
        return addFooterButton("YButton");
    }

    public GButton addFooterButton(GButton footerButton)
    {
        _showFooter = true;
        _pan_FooterBase.setVisible(_showFooter);
        footerButton.setPreferredSize(new Dimension(footerButton.getPreferredSize().width, 30));
        _pan_Footer.add(footerButton);

        return footerButton;
    }

    public GButton addFooterButton(String Text)
    {


        GButton btn_HeaderControl = new GButton(Text);
        btn_HeaderControl.setPreferredSize(new Dimension(100, 30));
        btn_HeaderControl.setForeground(GTheme.getColor(GTheme.C_TEXT_LIGHT_1));
        btn_HeaderControl.setBackground(GTheme.getColor(GTheme.C_OBJECT_BACKGROUND_LIGHT_1));
        _pan_Footer.add(btn_HeaderControl);

        return addFooterButton(btn_HeaderControl);
    }

    /*public GToggleButton addFooterToggleButton()
    {
        _showFooter = true;
        _pan_FooterBase.setVisible(_showFooter);

        GToggleButton btn_HeaderControl = new GToggleButton();
        btn_HeaderControl.setPreferredSize(new Dimension(100, 30));
        btn_HeaderControl.setForeground(GTheme.getColor(GTheme.C_TEXT_LIGHT_1));
        btn_HeaderControl.setBackground(GTheme.getColor(GTheme.C_TEXT_LIGHT_5));
        btn_HeaderControl.setPressedBackground(GTheme.getColor(GTheme.C_PRIMARY));
        _pan_Footer.add(btn_HeaderControl);

        return btn_HeaderControl;
    }
    */
    public void showFooter(boolean showFooter)
    {
        _showFooter = showFooter;
        _pan_FooterBase.setVisible(_showFooter);
    }

    public void showHeader(boolean showFooter)
    {
        _pan_Header.setVisible(_showFooter);
    }

    @Override
    public void remove(Component comp)
    {
        _pan_Content.remove(comp);
    }


    public void removeAll()
    {
        _pan_Content.removeAll();
    }

    public String getPaneID()
    {
        return _ID;
    }

    public String getPaneTitle()
    {
        return _title;
    }

    void setHeaderTitle(String title)
    {
        _ylbl_PageTitle.setText(title);
       // _pan_Header.setVisible(true);
    }


}