package GComponents;

import GComponents.external.WrapLayout;

import javax.swing.*;
import java.awt.*;

public class GScrollPane extends JScrollPane {
    JScrollBar _sbv;
    Container _cont = new Container();

    public GScrollPane() {

        setOpaque(false);
        setBorder(null);
        getVerticalScrollBar().setUnitIncrement(16);
        setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        getViewport().setView(_cont);
        getViewport().setOpaque(false);
        //getViewport().setLayout(new FlowLayout());

        _sbv = getVerticalScrollBar();
        _sbv.setUI(new GScrollbarUI());
        _sbv.setOpaque(false);

        _cont.setLayout(new WrapLayout());


    }

    public void ScrollToMaxmum() {
        getVerticalScrollBar();
        _sbv.setValue(_sbv.getMaximum());
    }

    @Override
    public Component add(Component component) {

        _cont.add(component);

        System.out.println("added");

        return component;
    }
}