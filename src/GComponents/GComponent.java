package GComponents;

import javax.swing.*;
import java.awt.*;

public abstract class GComponent extends JComponent
{
    //private Color _color;
    //private Shape _shape;
    public GComponent(){
        super();
        setSIZE(200,200);
    }
    private GAnimationTimer _animationTimer = new GAnimationTimer(0);

    // start internal methods

    private void setSIZE(int x, int y)
    {
        setPreferredSize(new Dimension(x, y));
        revalidate();
    }

    // end internal methods


    //start public methods | setters

    public void setWidth(int width)
    {
        setSIZE(width, getPreferredSize().height);
    }

    public void setHeight(int height)
    {
        setSIZE(getPreferredSize().width, height);
    }

    //public void setColor(Color color)
    //{
    //    _color = color;
    //}

    /*public void setShape(Shape shape)
    {
        _shape = shape;
    }*/

    @Override
    public void setLayout(LayoutManager mgr)
    {
        super.setLayout(mgr);
    }

    public void setAnimationTimer(GAnimationTimer timer)
    {
        _animationTimer = timer;
    }

    public void onAnimationStep(){};

    //end public methods | setters


    //start public methods | getters

    public int getWidth()
    {
        return super.getWidth();
    }

    public int getHeight()
    {
        return super.getHeight();
    }

    //public Color getColor()
    //{
    //    return _color;
    //}

    /* public Shape getShape()
    {
        return _shape;
    }*/

    public LayoutManager getLayout()
    {
        return super.getLayout();
    }

    public GAnimationTimer getAnimationTimer()
    {
        return _animationTimer;
    }

    //end public methods | getters

}
