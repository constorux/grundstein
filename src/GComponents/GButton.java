package GComponents;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GButton extends RoundedShape
{

	boolean isPressed = false;
	ActionListener _listener;
	GLabel _label;

	public GButton()
	{
		this(10);
	}
	
	public GButton(String Text)
	{
		this(10);
		setText(Text);
		
	}
	
	public GButton(int EdgeRound)
	{
		  
		this(EdgeRound, true, true, true, true);
	}
	    
    public GButton(int EdgeRound, Boolean LeftUpperRound, Boolean RightUpperRound, Boolean RightLowerRound ,Boolean LeftLowerRound)
    {
    	super(EdgeRound,LeftUpperRound,RightUpperRound, RightLowerRound, LeftLowerRound);

		setColor(GTheme.getColor(GTheme.C_PRIMARY));
	    

		_label = new GLabel();
		_label.setHorizontalAlignment(SwingConstants.CENTER);
		_label.setColor(GTheme.getColor(GTheme.C_WHITE));
		//_label.setMargin(new Insets(2, 2, 2, 2));




		setLayout(new BorderLayout());
		add(_label, BorderLayout.CENTER);
	  
    }





	public void setIcon(ImageIcon icon)
	{
		_label.setIcon(icon);
	}

	public void setText(String Text)
	{
		_label.setText(Text);
	}

	public String getText()
	{
		return _label.getText();
	}

	@Override
	public void onMouseClick() {
		if(_listener != null) {
			_listener.actionPerformed(new ActionEvent(this, 0, "Button pressed"));
		}
	}

	@Override
	public void onMousePressed() {
		setColor(GTheme.getColor(GTheme.C_ACCENT));
	}

	@Override
	public void onMouseReleased() {
		setColor(GTheme.getColor(GTheme.C_PRIMARY));
	}
	public void addActionListener(ActionListener listener){
		_listener = listener;
	}

}