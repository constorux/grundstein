package GComponents;


import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class GSettingsHeader extends GPanel {


    public GSettingsHeader(String SectionHeaderTitle, int textSize) {


        super(10);
        setOpaque(false);
        setHeight(70);

        setWidth(550);
        setBorder(new EmptyBorder(27, 7, 7, 7));
        setLayout(new BorderLayout(0, 0));


        GLabel lbl_Text = new GLabel();
        lbl_Text.setText(SectionHeaderTitle);
        lbl_Text.setFont(lbl_Text.getFont().deriveFont(20.0f));
        lbl_Text.setColor(GTheme.getColor(GTheme.C_TEXT_DARK_2));
        add(lbl_Text, BorderLayout.CENTER);

    }


}