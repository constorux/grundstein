package GComponents;

import GComponents.tools.GUtilities;

import java.awt.*;
import javax.swing.*;

public class GLabel extends GComponent
{

    JLabel _baseObject = new JLabel();
    int _width;

    public GLabel()
    {

        this("");

    }

    public GLabel(String text)
    {
        //_width = ((int) getPreferredSize().getWidth());
        //Dimension size = getPreferredSize();
        //size.width = size.height = Math.max(size.width, size.height);
        //setPreferredSize(new Dimension(10, 10));

        _baseObject.setText(text);
        _baseObject.setFont(GTheme.F_REGULAR(12));

        setWidth(100);
        setHeight(40);

        setOpaque(false);
        setLayout(new BorderLayout());
        this.add(_baseObject, BorderLayout.CENTER);
        setColor(GTheme.getColor(GTheme.C_TEXT_DARK_4));
    }

    public void setText(String Text)
    {
        _baseObject.setText("<html>" + Text + "</html>");
        validate();
    }

    public void setTextWidthSpecific(String Text)
    {
        _baseObject.setText("<html><body style='width: " + _width + "'>" + Text + "</body></html>");
        validate();
    }

    public void setTextNoHTML(String Text)
    {
        _baseObject.setText(Text);
        validate();
    }

    public void setIcon(ImageIcon icon, double scale)
    {
        _baseObject.setIcon(GUtilities.ShrinkImageToComponent(icon, this, scale));
    }

    public void setIcon(ImageIcon icon)
    {
        setIcon(icon, 1);
    }

    public void setHorizontalAlignment(int swingConstant)
    {
        _baseObject.setHorizontalAlignment(swingConstant);
    }

    public void setVerticalAlignment(int swingConstant)
    {
        _baseObject.setVerticalAlignment(swingConstant);
    }

    public void setHorizontalTextPosition(int swingConstant)
    {
        _baseObject.setHorizontalTextPosition(swingConstant);
    }

    public void setFont(Font f){
        _baseObject.setFont(f);
    }

    public Font getFont(){
        return _baseObject.getFont();
    }

    public String getText()
    {
        return _baseObject.getText();
    }

    public void setColor(Color color){
       _baseObject.setForeground(color);
    }
}