package GComponents;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class GTextField extends RoundedShape
{

	JTextField _baseObject = new JTextField();

	public GTextField()
	{	  
		this(10, true, true, true, true, "");
	}
	
	public GTextField(String text)
	{	  
		this(10, true, true, true, true, text);
	}
	
	public GTextField(int EdgeRound, Boolean LeftUpperRound, Boolean RightUpperRound, Boolean RightLowerRound ,Boolean LeftLowerRound, String text)
	{
		super(EdgeRound, LeftUpperRound, RightUpperRound,RightLowerRound, LeftLowerRound);

	    _baseObject.setText(text);
		_baseObject.setForeground(GTheme.getColor(GTheme.C_TEXT_DARK_2));
		_baseObject.setFont(GTheme.F_REGULAR(12));
		_baseObject.setBorder(null);
		_baseObject.setOpaque(false);
	    setBorder(new EmptyBorder(1, 10, 1, 10));

	    setWidth(200);

		setColor(GTheme.getColor(GTheme.C_OBJECT_BACKGROUND));

		//setOpaque(false);
		setLayout(new BorderLayout());
		this.add(_baseObject, BorderLayout.CENTER);
    }
  
  
    
  	public void setText(String text)
	{
		_baseObject.setText(text);
	}

	public String getText()
	{
		return _baseObject.getText();
	}

  
  
  
  
  
  
  
  
  
  
  
}