package GComponents;

import java.awt.*;

public class GPanel extends RoundedShape
{

	public GPanel()
	{	  
		this(0, false, false, false, false);
	 
	    
	}
	public GPanel(int EdgeRound){
		this(EdgeRound, true, true, true, true);
	}
	public GPanel(int EdgeRound, Boolean LeftUpperRound, Boolean RightUpperRound, Boolean RightLowerRound ,Boolean LeftLowerRound)
    {
		super(EdgeRound, LeftUpperRound, RightUpperRound, RightLowerRound, LeftLowerRound);

    	Dimension size = getPreferredSize();

	    setHeight(size.height);
	    setWidth(size.height = Math.max(size.width, size.height));

	    setColor(GTheme.getColor(GTheme.C_OBJECT_BACKGROUND));
	    setFont(GTheme.F_REGULAR(12));

	    
    }

}