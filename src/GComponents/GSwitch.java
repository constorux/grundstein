package GComponents;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.*;

public class GSwitch extends ShapeComponent
{

    String _text;
    Color mainColor = GTheme.getColor(GTheme.C_PRIMARY);

    boolean _isSelected;

    public GSwitch(){
        this(10);
    }

    //public GSwitch(boolean isSelected)
    //{
    //    this(10, isSelected);
    //}

    public GSwitch(int EdgeRound)
    {


        super();

        setSelected(false);
        //setSelected(isSelected);

        Dimension size = getPreferredSize();
        size.height = size.width / 2;

        setWidth(50);
        setHeight(31);

        setLayout(new BorderLayout());


    }

    @Override
    public void onMouseClick() {

        if(isSelected())
        {
            setSelected(false);
        }
        else
        {
            setSelected(true);
        }

    }


    //start methods | setters

    public void setSelected(boolean b)
    {
        if (b)
        {
            setAnimationTimer(new GAnimationTimer(getAnimationTimer().getCurrentValue(), getWidth() - (int) (getHeight()), this));
            _isSelected = true;

        } else
        {
            setAnimationTimer(new GAnimationTimer(getAnimationTimer().getCurrentValue(), 0, this));
            _isSelected = false;
        }
    }

    //end methods | setters

    //start methods | getters

    public boolean isSelected()
    {
        return _isSelected;
    }

    //end methods | getters


    @Override
    public void onAnimationStep()
    {
        if (_isSelected)
        {
            mainColor = (GTheme.getColor(GTheme.C_PRIMARY));
        } else
        {
            mainColor = (GTheme.getColor(GTheme.C_OBJECT_BACKGROUND_LIGHT_3));
        }


        repaint();
    }


    @Override
    ArrayList<Region> buildRegions()
    {
        ArrayList<Region> regions = new ArrayList<Region>();

        Area area = new Area(new RoundRectangle2D.Float(0, 0, getWidth(), getHeight(), getHeight() / 1, getHeight() / 1));
        area.subtract(new Area(new Ellipse2D.Double(getHeight() / 6 + getAnimationTimer().getCurrentValue(), getHeight() / 6, getHeight() * 0.6666 + 1, getHeight() * 0.666 + 1)));
        regions.add(new Region(area,mainColor));

        regions.add(new Region(new Area(new Ellipse2D.Double(getHeight() / 6 + getAnimationTimer().getCurrentValue(), getHeight() / 6, getHeight() * 0.6666 + 1, getHeight() * 0.666 + 1)), GTheme.getColor(GTheme.C_OBJECT_BACKGROUND_LIGHT_8)));


        return regions;
    }
}