package GComponents;


import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

public class GBase extends JFrame {

    JPanel _pan_Header_Controls;
    JPanel _pan_Content;
    JPanel _pan_Header;

    ArrayList<GContentPane> _childComponents = new ArrayList<GContentPane>();
    ArrayList<String> _childComponentNames = new ArrayList<String>();

    GContentPane _ycp_loadingPage;
    GLabel _ylbl_PageTitle;
    GButton _ybtn_closeApplication;

    GMenu _menu;

    int threadYPos;
    int threadXPos;


    public static int ANIMATION_UP = 0;
    public static int ANIMATION_DOWN = 1;
    public static int ANIMATION_LEFT = 2;
    public static int ANIMATION_RIGHT = 3;
    public static int ANIMATION_NONE = -1;

    //fullscreen
    static GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0];

    public GBase() {
        this("Yourcore Application");
    }

    public GBase(String Title) {

        Dimension size = getPreferredSize();
        setResizable(true);
        setSize(new Dimension(800,600));

        setLayout(new BorderLayout(0, 0));

        //DEBUG
        /*Toolkit temp = Toolkit.getDefaultToolkit();
        Dimension d = temp.getScreenSize();
        Image i = temp.getImage(GBase.class.getResource("/GComponents/resources/yc_logo.png"));
        setIconImage(i);*/

        //setIconImage(Toolkit.getDefaultToolkit().getImage(GBase.class.getResource("/GComponents/resources/yc_logo.png")));
        //setDefaultCloseOperation(HIDE_ON_CLOSE);
        //setBounds(100, 100, 850, 600);
        //setMinimumSize(new Dimension(400, 250));
        //setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        JPanel pan_Content_Base = new JPanel();
        add(pan_Content_Base, BorderLayout.CENTER);
        pan_Content_Base.setLayout(new BorderLayout(0, 0));

        _menu = new GMenu(this);
        add(_menu, BorderLayout.WEST);

        _pan_Content = new JPanel();
        pan_Content_Base.add(_pan_Content, BorderLayout.CENTER);
        _pan_Content.setLayout(new BorderLayout(0, 0));

        _pan_Header = new JPanel();
        _pan_Header.setPreferredSize(new Dimension(40, 40));
        _pan_Header.setBackground(GTheme.getColor(GTheme.C_CONTROL));
        pan_Content_Base.add(_pan_Header, BorderLayout.NORTH);
        _pan_Header.setLayout(new BorderLayout(0, 0));
        _pan_Header.setVisible(false);

        _ylbl_PageTitle = new GLabel("");
        _ylbl_PageTitle.setFont(GTheme.F_REGULAR(18));
        _ylbl_PageTitle.setForeground(GTheme.getColor(GTheme.C_TEXT_DARK_3));
        _ylbl_PageTitle.setPreferredSize(new Dimension(400, 40));
        _ylbl_PageTitle.setBorder(new EmptyBorder(7, 7, 7, 7));
        //ylbl_PageTitle.setBackground(GTheme.C_ACCENT);
        _pan_Header.add(_ylbl_PageTitle, BorderLayout.WEST);


        _pan_Header_Controls = new JPanel();
        _pan_Header_Controls.setPreferredSize(new Dimension(10, 40));
        _pan_Header_Controls.setOpaque(false);
        _pan_Header.add(_pan_Header_Controls, BorderLayout.CENTER);
        _pan_Header_Controls.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));

        _ybtn_closeApplication = new GButton("⨉");
        _ybtn_closeApplication.setBackground(GTheme.getColor(GTheme.C_PRIMARY));
        //_ybtn_closeApplication.setBorder(new EmptyBorder(5, 5, 5, 5));
        _ybtn_closeApplication.setEdgeRoundness(30);
        _ybtn_closeApplication.setPreferredSize(new Dimension(40, 40));
        _ybtn_closeApplication.setRoundEdges(false, false, false, true);
        _ybtn_closeApplication.setVisible(true);
        _pan_Header.add(_ybtn_closeApplication, BorderLayout.EAST);


        //TODO loading page

        _ycp_loadingPage = new GContentPane(true, "loading", "");
        //_ycp_loadingPage.setBounds(12, 12, 755, 421);
        _ycp_loadingPage.setOpaque(true);
        _ycp_loadingPage.showFooter(false);
        addContent(_ycp_loadingPage, false);
        //_ycp_loadingPage.setContentLayout(new BorderLayout(0, 0));

        GLabel ylbl_loadingText = new GLabel();
        ylbl_loadingText.setIcon(new ImageIcon(GLabel.class.getResource("/GComponents/resources/grundstein_logo.png")), 2);
        ylbl_loadingText.setHorizontalAlignment(SwingConstants.CENTER);
        //ylbl_loadingText.setFont(GTheme.F_BOLD(35));
        //ylbl_loadingText.setForeground(GTheme.getColor(GTheme.C_TEXT_LIGHT_5));
        //ylbl_loadingText.setHorizontalAlignment(SwingConstants.CENTER);
        _ycp_loadingPage.add(ylbl_loadingText, SwingConstants.CENTER);


        //start
        setEnabled(true);
        showLoadingPage();
        setTitle(Title);



        //action listeners

        JFrame _this = this;
        _ybtn_closeApplication.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispatchEvent(new WindowEvent(_this, WindowEvent.WINDOW_CLOSING));
            }
        });


    }


//  public GButton addHeaderBackButton()
//  {
//	  	GButton ybtn_HeaderBack = new GButton(30, false, true, true, false);
//	  	ybtn_HeaderBack.setPreferredSize(new Dimension(40, 40));
//	  	ybtn_HeaderBack.setBorder(new EmptyBorder(7, 7, 7, 7));
//	  	ybtn_HeaderBack.setBackground(GTheme.C_ACCENT);
//	  	ybtn_HeaderBack.setIcon(new ImageIcon( GBase.class.getResource("/GComponents/resources/yc_button_back.png")), 0.7);
//		_pan_Header.add(ybtn_HeaderBack, BorderLayout.WEST);
//		
//		return (ybtn_HeaderBack);
//  }

    public void clearHeaderControls() {
        _pan_Header_Controls.removeAll();
        repaint();
    }


    public GButton addHeaderButton() {
        return addHeaderButton("GButton");
    }

    public GButton addHeaderButton(String Text) {
        GButton btn_HeaderControl = new GButton(Text);
        btn_HeaderControl.setPreferredSize(new Dimension(100, 30));
        btn_HeaderControl.setForeground(GTheme.getColor(GTheme.C_TEXT_LIGHT_1));
        btn_HeaderControl.setBackground(GTheme.getColor(GTheme.C_ACCENT));
        _pan_Header_Controls.add(btn_HeaderControl);

        return btn_HeaderControl;
    }

    public GButton addHeaderButton(GButton b) {
        b.setPreferredSize(new Dimension(b.getPreferredSize().width, 30));
        _pan_Header_Controls.add(b);

        return b;
    }

    public void addContent(GContentPane Item) {
        addContent(Item, true);
    }

    public void addContent(GContentPane Item, boolean addToMenu) {
        addContent(Item, addToMenu, null);
    }

    public void addContent(GContentPane Item, boolean addToMenu, ImageIcon menuIcon) {
        if (Item != null) {
            _childComponents.add(Item);
            _childComponentNames.add(Item.getPaneID());
            // _pan_Content.add(Item, Name);
            if (addToMenu) {
                _menu.addMenuItem(new GMenuItem(Item, menuIcon));
            }
        }


    }

    @Override
    public Component add(Component Item) {
        if (Item.getClass() == GContentPane.class) {
            addContent(((GContentPane) Item));
        } else {
            System.out.println("component type not supported");
        }

        //addMenuItem(new GMenuItem(paneName));

        return Item;
    }

    public void isBETA(boolean IsBeta) {
        _menu.isBeta(IsBeta);
    }

    public void showContent(String Name) {
        showContent(Name, ANIMATION_UP);
    }

    public void showContent(String Name, int Animation) {

        if (Animation == 0) {
            threadYPos = 20;
            threadXPos = 0;
        } else if (Animation == 1) {
            threadYPos = -20;
            threadXPos = 0;
        } else if (Animation == 2) {
            threadYPos = 0;
            threadXPos = -20;
        } else if (Animation == 3) {
            threadYPos = 0;
            threadXPos = 20;
        } else {
            threadYPos = 0;
            threadXPos = 0;
        }

        for (int i = 0; i < _childComponentNames.size(); i++) {
            if (_childComponentNames.get(i).equals(Name)) {
                //System.out.println(Name);
                GContentPane pane = _childComponents.get(i);
                //pane.setLocation(0, 0);
                _pan_Content.removeAll();
                _pan_Content.add(pane, BorderLayout.CENTER);
                revalidate();

                Timer transitionTimer = new Timer(10, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent TransitionActionEvent) {
                        if ((threadYPos == 0) && (threadXPos == 0)) {
                            _pan_Content.setBackground(new Color(255, 255, 255));
                            repaint();
                            revalidate();

                            ((Timer) TransitionActionEvent.getSource()).stop();
                        } else {
                            _pan_Content.setBackground(new Color(95 + (Math.abs(threadYPos + threadXPos) * 8), 95 + (Math.abs(threadYPos + threadXPos) * 8), 95 + (Math.abs(threadYPos + threadXPos) * 8)));
                            //_pan_Content.setBackground(new Color(15 + (threadYPos * 12),15 + (threadYPos * 12),15 + (threadYPos * 12)));
                            pane.setLocation(threadXPos, threadYPos);
                            repaint();
                        }

                        if (threadYPos != 0) {
                            if (threadYPos > 0) {
                                threadYPos--;
                            } else {
                                threadYPos++;
                            }
                        }

                        if (threadXPos != 0) {
                            if (threadXPos > 0) {
                                threadXPos--;
                            } else {
                                threadXPos++;
                            }
                        }
                    }
                });
                transitionTimer.start();
                return;
            }
        }
        System.out.println("ERROR! page with name <" + Name + ">does not exist");

    }

    public void showLoadingPage() {
        showContent("loading");
    }

    public void showMenu(boolean show) {
        _menu.showMenu(show);
    }

    public void showHeader(boolean show) {
        _pan_Header.setVisible(show);
    }


    public void setFullscreen(boolean fullscreen) {
        _ybtn_closeApplication.setVisible(fullscreen);
        if(fullscreen){
            showHeader(true);}

        if (fullscreen) {
            device.setFullScreenWindow(this);
        } else {
            device.setFullScreenWindow(null);
        }
    }


    //internal voids
    void setHeaderTitle(String title) {
        _ylbl_PageTitle.setText(title);
        showHeader(true);
    }

    public void setTitle(String Title) {
        _menu.setTitle(Title);
        super.setTitle(Title);
    }


}


class GMenu extends GPanel {

    GBase _base;

    JPanel _pan_MenuItems;
    GLabel _lbl_MenuHeader_Title;
    GMenuItem _pan_MenuFooter;

    public GMenu(GBase base) {
        _base = base;


        setAnimationTimer(new GAnimationTimer(40));
        setWidth(getAnimationTimer().getCurrentValue());
        setLayout(new BorderLayout(0, 0));
        setColor(GTheme.getColor(GTheme.C_OBJECT_BACKGROUND_LIGHT_3));

        JPanel pan_MenuHeader = new JPanel();
        pan_MenuHeader.setPreferredSize(new Dimension(180, 40));
        pan_MenuHeader.setBackground(GTheme.getColor(GTheme.C_MENU_HEADER));
        pan_MenuHeader.setMinimumSize(new Dimension(180, 10));
        add(pan_MenuHeader, BorderLayout.NORTH);
        pan_MenuHeader.setLayout(new BorderLayout(0, 0));

        _lbl_MenuHeader_Title = new GLabel();
        _lbl_MenuHeader_Title.setColor(GTheme.getColor(GTheme.C_TEXT_LIGHT_4));
        _lbl_MenuHeader_Title.setBorder(new EmptyBorder(0, 10, 0, 0));
        pan_MenuHeader.add(_lbl_MenuHeader_Title, BorderLayout.CENTER);

        JPanel pan_Menu_Body = new JPanel();
        pan_Menu_Body.setBackground(GTheme.getColor(GTheme.C_MENU_BODY));
        add(pan_Menu_Body, BorderLayout.CENTER);
        pan_Menu_Body.setLayout(new BorderLayout(0, 0));

        JPanel pan_Menu_Body_Seperator = new JPanel();
        pan_Menu_Body_Seperator.setOpaque(false);
        pan_Menu_Body_Seperator.setPreferredSize(new Dimension(10, 3));
        pan_Menu_Body_Seperator.setBackground(GTheme.getColor(GTheme.C_PRIMARY));
        pan_Menu_Body.add(pan_Menu_Body_Seperator, BorderLayout.NORTH);

        GMenuButton btn_MenuHeader_Btn = new GMenuButton();
        btn_MenuHeader_Btn.setPreferredSize(new Dimension(40, 40));
        pan_MenuHeader.add(btn_MenuHeader_Btn, BorderLayout.EAST);

        _pan_MenuItems = new JPanel();
        _pan_MenuItems.setOpaque(false);
        pan_Menu_Body.add(_pan_MenuItems, BorderLayout.CENTER);
        _pan_MenuItems.setLayout(new FlowLayout(FlowLayout.RIGHT, 0, 2));

        _pan_MenuFooter = new GMenuItem(null, "BETA", new ImageIcon(GBase.class.getResource("/GComponents/resources/beta-icon.png")));
        _pan_MenuFooter.setColor(GTheme.getColor(GTheme.C_ACCENT));
        add(_pan_MenuFooter, BorderLayout.SOUTH);
        _pan_MenuFooter.setVisible(false);



        showMenu(true);



        GMenu thisMenu = this;
        btn_MenuHeader_Btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (getWidth() < 180) {
                    setAnimationTimer(new GAnimationTimer(getWidth(), 180, thisMenu));
                } else {
                    setAnimationTimer(new GAnimationTimer(getWidth(), 40, thisMenu));
                }


            }
        });
    }

    public void showMenu(boolean show) {
        setVisible(show);
    }

    public void setTitle(String title) {
        _lbl_MenuHeader_Title.setText(title);
    }

    public void isBeta(boolean beta) {
        _pan_MenuFooter.setVisible(beta);
    }

    public void clearMenuItems() {
        _pan_MenuItems.removeAll();
        repaint();
    }

    public void addMenuItem(GMenuItem MenuItem) {
        _pan_MenuItems.add(MenuItem);
        revalidate();

        MenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                _base.showContent(MenuItem.getID());

            }
        });
    }

    @Override
    public void onAnimationStep() {
        setWidth(getAnimationTimer().getCurrentValue());
    }
}