package GComponents;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Area;
import java.util.ArrayList;

public abstract class ShapeComponent extends GComponent {

    private boolean _opaque = true;


    public ShapeComponent() {
        // super();

        //setPreferredSize(new Dimension(300, 200));

        setWidth(120);
        setHeight(40);

        //setOpaque(true);
        //setBorder(new BevelBorder(BevelBorder.RAISED));


        super.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (doesContain(e.getX(), e.getY())) {
                    onMouseClick();
                }
            }
            @Override
            public void mousePressed(MouseEvent e) {
                if (doesContain(e.getX(), e.getY())) {
                    onMousePressed();
                }
            }
            @Override
            public void mouseReleased(MouseEvent e) {
                if (doesContain(e.getX(), e.getY())) {
                    onMouseReleased();
                }
            }
        });
    }


    @Override
    public void paintComponent(Graphics g) {


        Graphics2D g2d = (Graphics2D) g.create();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        if (_opaque) {

            for (Region region : buildRegions()) {
                if ((region != null) && (region.getColor() != null) && (region.getShape() != null)) {
                    g2d.setColor(region.getColor());
                    g2d.fill(region.getShape());
                }
            }

        }
        g2d.dispose();

        super.paintComponent(g);
    }


    ArrayList<Region> buildRegions() {

        ArrayList<Region> regions = new ArrayList<Region>();
        regions.add(new Region(new Area(new Rectangle(0, 0, getWidth(), getHeight())), GTheme.getColor(GTheme.C_OBJECT_BACKGROUND)));
        return (regions);
    }


    public void onMouseClick() {

    }

    public void onMousePressed() {

    }

    public void onMouseReleased() {

    }


    private boolean doesContain(int x, int y) {

        for (Region region : buildRegions()) {
            if ((region != null) && (region.doesContain(x, y))) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void setOpaque(boolean opaque)
    {
        _opaque = opaque;
    }

}


class Region {
    private Shape _shape;
    private Color _color;

    public Region(Shape shape, Color color) {
        _shape = shape;
        _color = color;
    }

    public void setShape(Shape shape) {
        _shape = shape;
    }

    public void setColor(Color color) {
        _color = color;
    }

    public Shape getShape() {
        return _shape;
    }

    public Color getColor() {
        return _color;
    }

    public boolean doesContain(int x, int y) {
        if ((_shape != null) && (_shape.contains(x, y))) {
            return true;
        }
        return false;
    }
}
