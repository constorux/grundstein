package GComponents;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JComponent;

public class GAnimationTimer
{
	private double _value;

	private int _waitTime = 1; //hardcoded for now
	private double _stepSize = 3;
	private boolean _upwards;

	public GAnimationTimer(int value)
	{
		this(value, value);
	}

	public GAnimationTimer(int value, int toValue)
	{
		this(value, toValue, 40, null);
	}

	public GAnimationTimer(int value, int toValue, GComponent component)
	{
		this(value, toValue, 40, component);
	}

	public GAnimationTimer(int value, int toValue, int TimeToComplete, final GComponent component)
	{

		_value = value;
		_upwards = false;

		if(TimeToComplete > 0)
		{
			_stepSize = (double)Math.abs(_value - toValue)/(double)TimeToComplete;
			if(_stepSize <= 0)
			{
				_stepSize = 0.01;
			}

		}


		if(value < toValue)
		{
			_upwards = true;
		}

		Timer t = new Timer();
		t.schedule(new TimerTask()
		{
		    @Override
		    public void run()
			{
			    if(_upwards)
			    	{
			    		_value += _stepSize;

			    		if(_value >= toValue)
			    		{
			    			_value = toValue;
			    		}
			    	}
			    	else
			    	{
			    		_value -= _stepSize;

			    		if(_value <= toValue)
			    		{
			    			_value = toValue;
			    		}
			    	}


			    	if(component != null)
		    		{
						component.onAnimationStep();
		    		}

			    	if(_value == toValue){
			    	    t.cancel();
                    }
			}

		},0, 7);


	}

	public int getCurrentValue()
	{
		return (int) Math.round(_value);
	}

	//legacy
	/*public void setCurrentValue(int value)
	{
	    _value = value;
	}*/

}
