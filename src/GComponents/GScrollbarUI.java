package GComponents;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.metal.MetalScrollBarUI;

public class GScrollbarUI extends MetalScrollBarUI {

    private JButton b = new JButton() {

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(0, 0);
        }

    };

    public GScrollbarUI() {

    }

    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
        g.setColor(GTheme.getColor(GTheme.C_OBJECT_BACKGROUND_LIGHT_3));
        g.fillRoundRect (r.x + (r.width/2 - 3),  r.y + 5, 6, r.height - 10, 2,2);
        //.drawImage(imageThumb, null);
    }

    @Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle r) {
        g.setColor(GTheme.getColor(GTheme.C_OBJECT_BACKGROUND_LIGHT_7));
        //g.fillRect (r.x ,  r.y , r.width, r.height);
    }

    @Override
    protected JButton createDecreaseButton(int orientation) {
        return b;
    }

    @Override
    protected JButton createIncreaseButton(int orientation) {
        return b;
    }
}
