package main;


import GComponents.*;
import GComponents.external.WrapLayout;
import YComponentsLegacy.YToggleButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class start {

	public static void main(String[] args) 
	{

		GBase bas_base = new GBase("Grundstein showcase");

		GContentPane ctp_mainContent = new GContentPane("main_content", "Overview");
		ctp_mainContent.setPreferredSize(new Dimension(10, 40));
		ctp_mainContent.addFooterButton("Footer button");
		ctp_mainContent.setLayout(new WrapLayout());
		bas_base.addContent(ctp_mainContent, true);

        GContentPane ctp_settings = new GContentPane("settings", "settings");
        bas_base.addContent(ctp_settings, true, new ImageIcon(GMenuItem.class.getResource("/GComponents/resources/yc_menu_settings.png")));

		GPanel pan_credentials = new GPanel(20);
		pan_credentials.setColor(GTheme.getColor(GTheme.C_OBJECT_BACKGROUND_LIGHT_7));
		pan_credentials.setLayout(new WrapLayout());
		pan_credentials.setWidth(320);
		pan_credentials.setHeight(100);
		ctp_mainContent.add(pan_credentials);

		pan_credentials.add(new GLabel("username: "));
        pan_credentials.add(new GTextField());
        pan_credentials.add(new GLabel("password: "));
        pan_credentials.add(new GTextField());


		bas_base.addHeaderButton(new GButton("global header"));
		bas_base.showHeader(true);



        ctp_mainContent.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));

		GButton btn_resizeBtn = new GButton("resize GButton");
        ctp_mainContent.add(btn_resizeBtn);

        ctp_mainContent.add(new GButton("GButton"));
        ctp_mainContent.add(new GTextField("GTextField"));
        ctp_mainContent.add(new GLabel("GLabel"));
        ctp_mainContent.add(new GSwitch());

		GScrollPane scrollPane = new GScrollPane();
        ctp_settings.add(scrollPane);

		scrollPane.add(new GSettingsHeader("settings example", 20));
		scrollPane.add(new GSettingsItem("this is a SettingsItem showing a GSwitch", new GSwitch()));
		scrollPane.add(new GSettingsItem("SettingsItem can take custom components", new GTextField()));

		scrollPane.add(new GSettingsHeader("simple scrolling is also supported", 20));
		for(int i = 0; i <= 15; i++){
			scrollPane.add(new GSettingsItem("Test settings item number " + i, new GSwitch()));
		}

		bas_base.setVisible(true);

		btn_resizeBtn.addActionListener( new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
                btn_resizeBtn.setWidth((int)(Math.random() * 200 + 100));
                btn_resizeBtn.setHeight((int)(Math.random() * 200 + 40));

				btn_resizeBtn.setColor(new Color((int) (Math.random() * 255),(int) (Math.random() * 255),(int) (Math.random() * 255)));
                btn_resizeBtn.setText(btn_resizeBtn.getWidth() + "px x " + btn_resizeBtn.getHeight() + "px");
				//JOptionPane.showMessageDialog(null, "Feedback works");
			}
		});



	}
}
