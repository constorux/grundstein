<div align="right"><img src="https://gitlab.com/constorux/grundstein/raw/master/media/grundstein-logo-transparent.png" alt="Grundstein logo" width="200"/></div>

# Grundstein Toolkit
1. [About](#about)
2. [Download](#download)
3. [Features](#features)
4. [Usage](#usage)
5. [Screenshots](#screenshots)

## About

Grundstein is a user interface toolkit based on Java Swing. It draws its 
components using the JComponent framework and is therefore compatible with 
JComponent components. Grundstein aimes to make it easier for developers to 
create good looking and intuitive user experiences without having to learn a 
new toolkit.

## Download

You can download the **latest stable version of Grundstein** **[here](https://gitlab.com/constorux/grundstein/raw/master/releases/GrundsteinToolkit.jar)**.

Import the Library in your favorite IDE and import the classes that you want to
use. **Thank you for considering my project!**

## Features

- Grundstein components can be themed in various ways and offer many additional 
methods for customizing their looks. 
- Components offer an easy way to create smooth animations
- Grundstein offers additional components which extend the functionality of
exisiting Swing components
- existing Swing components can be used alongside the Grundstein components

## Usage

Opening a basic window in Grundstein

```java
import GComponents.GBase;
import GComponents.GContentPane;
import GComponents.GLabel;

public class Main {

    public static void main(String[] args) {

        //creating a new Window with the Title "Hello World Application"
        GBase base = new GBase("Hello World Application");

        //adding a ContentPane. These are used to display Content in the Window
        //The ID (hello_world_pane) is used to tell the Window which ContentPane to display
        GContentPane contentPane = new GContentPane("hello_world_pane", "Hello World Pane");

        //adding the ContentPane to the Window and telling it to add it to the menu
        base.addContent(contentPane, true);

        //Creating a new TextLabel and adding it to the ContentPane
        GLabel label = new GLabel("Hello World");
        label.setHorizontalAlignment(0);
        contentPane.add(label);

        //showing the above defined ContentPane and rendering the Window on screen
        base.showContent("hello_world_pane");
        base.setVisible(true);

    }
}
```

## Screenshots

The basic application layout

![default state](https://gitlab.com/constorux/grundstein/raw/master/grundstein-showcase/grundstein-showcase-basic.png)

An overview over a few different components

![components overview](https://gitlab.com/constorux/grundstein/raw/master/grundstein-showcase/grundstein-showcase-main.png)

A possible settings page

![settings page](https://gitlab.com/constorux/grundstein/raw/master/grundstein-showcase/grundstein-showcase-settings.png)


#### Written with ♡